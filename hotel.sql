/*
SQLyog Ultimate v8.32 
MySQL - 5.7.10-log : Database - hotel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hotel` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hotel`;

/*Table structure for table `reservation` */

DROP TABLE IF EXISTS `reservation`;

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_in` date NOT NULL,
  `date_out` date NOT NULL,
  `room_type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `persons_number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `status_idx` (`status`),
  KEY `room_type_idx` (`room_type`),
  CONSTRAINT `reservation_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `status` FOREIGN KEY (`status`) REFERENCES `reservation_status` (`id`),
  CONSTRAINT `type_of_room` FOREIGN KEY (`room_type`) REFERENCES `room_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

/*Data for the table `reservation` */

/*Table structure for table `reservation_room` */

DROP TABLE IF EXISTS `reservation_room`;

CREATE TABLE `reservation_room` (
  `id_reservation` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  KEY `reservation_room_room_id_fk` (`id_room`),
  KEY `reservation_room_reservation_id_fk` (`id_reservation`),
  CONSTRAINT `reservation_room_reservation_id_fk` FOREIGN KEY (`id_reservation`) REFERENCES `reservation` (`id`) ON DELETE CASCADE,
  CONSTRAINT `reservation_room_room_id_fk` FOREIGN KEY (`id_room`) REFERENCES `room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `reservation_room` */

/*Table structure for table `reservation_status` */

DROP TABLE IF EXISTS `reservation_status`;

CREATE TABLE `reservation_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `reservation_status` */

insert  into `reservation_status`(`id`,`status_name`) values (1,'IN_PROCESS'),(2,'CONFIRMED');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role`) values (1,'admin'),(2,'user');

/*Table structure for table `room` */

DROP TABLE IF EXISTS `room`;

CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `room_number_uindex` (`number`),
  KEY `room_room_type_id_fk` (`type`),
  CONSTRAINT `room_room_type_id_fk` FOREIGN KEY (`type`) REFERENCES `room_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `room` */

insert  into `room`(`id`,`number`,`type`) values (9,100,1),(10,101,1),(13,301,3),(14,201,2),(15,202,2),(16,302,3),(17,102,1),(18,203,2),(19,204,2),(20,205,2),(22,207,2),(23,206,2);

/*Table structure for table `room_type` */

DROP TABLE IF EXISTS `room_type`;

CREATE TABLE `room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `max_capacity` int(11) NOT NULL,
  `name` varchar(10) NOT NULL,
  `cost` int(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `room_type` */

insert  into `room_type`(`id`,`max_capacity`,`name`,`cost`) values (1,1,'single',10),(2,2,'double',20),(3,5,'family',50);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`email`,`password`) values (23,'Stepan','steve@gmail.com','$2a$10$jn/SHdGVuK7Qai11x6l8zep4N9/cMSjXP9jzswF8GDoa0jMd22UK2'),(24,'Stepan','qwery@gmail.com','$2a$10$lRMUx7QrLsKInr3EDQKP5eUktdAq1bgsFvl8HBZFBuQVvCvlizhB.'),(27,'Elise','bipyc_h@mail.ru','$2a$10$d0sNcABHRnQjsHZ0qU5oJubA5XEDsYh3GKrNUOIZrnmLApP9taF/y'),(29,'GitTest','qqq@gmail.com','$2a$10$bW1bXmglz9OLEAa9WErd5ulZLMkNZF7M46fYxN4KgWWD1GOKEMhym'),(36,'Elise','tsybulskystepan@gmail.com','$2a$10$ZWWny0/mRzi.xiSG6ZNQWuMaAFqmy1QBaO0LrXGi8/8ZIGjLr8.kG'),(38,'Дмитрий','test2@gmail.com','$2a$10$64vK9Dhu4c0SK7gvyNhXmeEMgFaDnXuce5I9pSX8rJTDLNvej9TBC');

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  UNIQUE KEY `user_role_id_user_uindex` (`id_user`),
  KEY `user_role_roles_id_fk` (`id_role`),
  CONSTRAINT `user_role_roles_id_fk` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

insert  into `user_role`(`id_user`,`id_role`) values (23,1),(24,2),(27,2),(29,2),(36,2),(38,2);

/* Trigger structure for table `user` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `user_after_insert` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `user_after_insert` AFTER INSERT ON `user` FOR EACH ROW BEGIN
    INSERT INTO user_role VALUES (NEW.id, (SELECT id FROM roles WHERE role='user'));
  END */$$


DELIMITER ;

/* Trigger structure for table `user` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `user_after_delete` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `user_after_delete` AFTER DELETE ON `user` FOR EACH ROW BEGIN
    DELETE FROM user_role WHERE user_role.id_user=old.id;
  END */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
