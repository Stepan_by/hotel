package by.epam.training.manager;

import by.epam.training.enums.Pages;
import org.junit.Before;
import org.junit.Test;

import static by.epam.training.enums.Pages.*;
import static org.junit.Assert.*;

public class PagesManagerTest {

    private String pagePath;

    @Before
    public void setUp() throws Exception {
        pagePath = "/jsp/home.jsp";
    }

    @Test
    public void testGetPage() throws Exception {
        Pages mainPage = MAIN;
        String result = PagesManager.getPage(mainPage.getPageInProperty());
        assertEquals(pagePath, result);
    }
}