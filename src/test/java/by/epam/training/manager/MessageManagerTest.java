package by.epam.training.manager;

import by.epam.training.enums.Messages;
import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.*;

public class MessageManagerTest {

    private Locale locale;
    private String messagePath;

    @Before
    public void setUp() throws Exception {
        locale = new Locale("en_US");
        messagePath = Messages.EMAIL_ERROR.getMessageInProperty();
    }

    @Test
    public void testChangeLocale() throws Exception {
        String messageBefore = MessageManager.getMessage(messagePath);
        assertEquals(messageBefore, "Такой email уже существует");
        MessageManager.changeLocale(locale);
        String messageAfter = MessageManager.getMessage(messagePath);
        assertEquals(messageAfter, "This email already exists");
    }
}