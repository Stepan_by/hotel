package by.epam.training.hasher;

import org.junit.Before;
import org.junit.Test;
import org.mindrot.jbcrypt.BCrypt;

import static org.junit.Assert.*;

public class HasherTest {

    private String password;

    @Before
    public void setUp() throws Exception {
        password = "password";
    }

    @Test
    public void testIsPasswordsEquals() throws Exception {
        String passwordHash = BCrypt.hashpw(password, BCrypt.gensalt());
        assertTrue(Hasher.isPasswordsEquals(password, passwordHash));
    }
}