package by.epam.training.servlet;

import by.epam.training.command.factory.CommandFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class is the only one "front" servlet in the whole application.
 * All requests pass through it and they must have an attribute "command"
 * which value is used for invoking the correspond command.
 */
@WebServlet("/controller")
public class DispatcherServlet extends HttpServlet {

    public static final String MESSAGE = "message";

    /**
     *GET requests processor.
     *It just invokes the {@code processRequest} method.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /**
     *POST requests processor.
     *It just invokes the {@code processRequest} method.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /**
     * Method executes the arrived command.
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().removeAttribute(MESSAGE);
        CommandFactory.createCommand(request, response).execute();
    }
}
