package by.epam.training.hasher;

import org.mindrot.jbcrypt.BCrypt;

/**
 * Class for hashing the password
 * and comparing given passwords with hashed
 */
public class Hasher {

    /**
     * Hashing the password
     * @param password that we are hashing
     * @return hashed password
     */
    public static String hashPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    /**
     * Comparing the passwords
     * @param candidatePassword given password
     * @param storedHash hashed password
     * @return true if candidate password equals hashed password
     */
    public static boolean isPasswordsEquals(String candidatePassword, String storedHash) {
        return BCrypt.checkpw(candidatePassword, storedHash);
    }
}
