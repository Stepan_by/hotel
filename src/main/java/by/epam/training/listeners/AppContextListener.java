package by.epam.training.listeners;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.File;

/**
 * Listener for initialization logger
 */
@WebListener
public class AppContextListener implements ServletContextListener {

    public static final String LOG4J_ATTRIBUTE = "log4j-config";
    public static final Logger LOGGER = Logger.getLogger(AppContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        loggerInitialization(servletContextEvent.getServletContext());
        LOGGER.info("Logger Log4J initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

    private void loggerInitialization(ServletContext context) {
        String log4jConfigPath = context.getInitParameter(LOG4J_ATTRIBUTE);
        if(log4jConfigPath != null){
            String log4jProp = context.getRealPath("/") + log4jConfigPath;
            File log4jConfigFile = new File(log4jProp);
            if (log4jConfigFile.exists()) {
                DOMConfigurator.configure(log4jProp);
            }
        }
    }
}
