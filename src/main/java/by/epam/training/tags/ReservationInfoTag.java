package by.epam.training.tags;

import by.epam.training.entities.Reservation;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Custom tag
 */
public class ReservationInfoTag extends TagSupport {

    private Reservation reservation;
    private boolean showDate = false;

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public void setShowDate(boolean showDate) {
        this.showDate = showDate;
    }

    @Override
    public int doStartTag() throws JspException {
        JspWriter writer = pageContext.getOut();
        try {
            writer.write("<th>" + reservation.getId() + "</th>");
            writer.write("<th>" + reservation.getType() + "</th>");
            writer.write("<th>" + reservation.getNumberOfPersons() + "</th>");
            if (showDate) {
                writer.write("<th>" + reservation.getDateFrom() + "</th>");
                writer.write("<th>" + reservation.getDateTo() + "</th>");
            }
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
