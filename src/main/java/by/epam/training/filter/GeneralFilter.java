package by.epam.training.filter;

import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * General filter for setting the encoding,
 * and setting the general session attributes
 */
@WebFilter(filterName="GeneralFilter", urlPatterns="/*")
public class GeneralFilter implements Filter {

    private Logger LOGGER = Logger.getLogger(GeneralFilter.class);

    private HttpServletRequest request;
    private HttpServletResponse response;

    //Attributes
    public static final String USER = "user";
    public static final String LOCALE = "locale";
    public static final String ENCODING = "UTF-8";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("GeneralFilter initialized");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        request = (HttpServletRequest) servletRequest;
        response = (HttpServletResponse) servletResponse;
        LOGGER.info("Requested Resource is " + request.getRequestURI());
        changeEncoding();
        configSession();
        filterChain.doFilter(servletRequest, servletResponse);
    }

    /**
     * Setting general session's attributes
     */
    private void configSession() {
        HttpSession session = request.getSession();
        if (session.isNew()) {
            session.setAttribute(USER, new User());
            session.setAttribute(LOCALE, request.getLocale());
        }
    }

    /**
     * Changing the encoding
     */
    private void changeEncoding() {
        try {
            request.setCharacterEncoding(ENCODING);
            response.setCharacterEncoding(ENCODING);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Can't change the encoding");
        }
    }

    @Override
    public void destroy() {
        LOGGER.info("General filter destroyed");
    }
}
