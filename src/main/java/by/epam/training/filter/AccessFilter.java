package by.epam.training.filter;

import by.epam.training.enums.Pages;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter for checking the access to the resources
 */
@WebFilter(filterName="AccessFilter", urlPatterns="/*")
public class AccessFilter implements Filter {

    private Logger LOGGER = Logger.getLogger(GeneralFilter.class);

    private HttpServletRequest request;
    private HttpServletResponse response;

    //Pages
    public static final String MAIN = Pages.MAIN.getPageInProperty();
    public static final String LOGIN = Pages.LOGIN.getPageInProperty();

    //Attributes
    public static final String ADMIN = "admin";
    public static final String USER = "user";
    public static final String COMMAND = "command";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("Access filter initialized");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        request = (HttpServletRequest) servletRequest;
        response = (HttpServletResponse) servletResponse;
        if (isAccessAllowed()) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    /**
     * Check the user's rights and
     * allowing or denying the access to the resource
     * @return true if the access is allowed
     * @throws IOException
     */
    private boolean isAccessAllowed() throws IOException {
        User user = (User) request.getSession().getAttribute(USER);
        String command = request.getParameter(COMMAND);
        String requestedResource = request.getRequestURI();
        switch (user.getRole()) {
            case GUEST:
                return checkGuestRights(requestedResource, command);
            case USER:
                return checkUserRights(requestedResource, command);
            case ADMIN:
                return true;
            default:
                return false;
        }
    }

    /**
     * Checking the Guest's rights
     * @param resource requested resource
     * @param command invoked command
     * @return true if the access allowed
     * @throws IOException
     */
    private boolean checkGuestRights(String resource, String command) throws IOException {
        if (resource.contains(USER) || resource.contains(ADMIN)) {
            response.sendRedirect(PagesManager.getPage(LOGIN));
            return false;
        }
        if (command != null) {
            if (command.contains(ADMIN) || command.contains(USER)) {
                response.sendRedirect(PagesManager.getPage(LOGIN));
                return false;
            }
        }
        return true;
    }

    /**
     * Checking the User's rights
     * @param resource requested resource
     * @param command invoked command
     * @return true if the access allowed
     * @throws IOException
     */
    private boolean checkUserRights(String resource, String command) throws IOException {
        if (resource.contains(ADMIN)) {
            response.sendRedirect(PagesManager.getPage(MAIN));
            return false;
        }
        if (command != null) {
            if (command.contains(ADMIN)) {
                response.sendRedirect(PagesManager.getPage(MAIN));
                return false;
            }
        }
        return true;
    }

    @Override
    public void destroy() {
        LOGGER.info("Access filter destroyed");
    }
}
