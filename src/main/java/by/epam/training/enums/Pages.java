package by.epam.training.enums;

/**
 * Enum for linking the pages
 */
public enum Pages {

    MAIN("main"),

    //User
    BOOKING("user.booking"),
    USER_BOOKINGS("user.bookings"),
    LOGIN("user.login"),
    PAYMENT("user.payment"),
    REGISTRATION("user.registration"),
    SETTINGS("user.settings"),
    SUCCESS("success"),

    //Admin
    ADMIN_BOOKINGS("admin.bookings"),
    BOOK_INFO("admin.book_info"),
    ROLES("admin.roles"),
    ROOMS("admin.rooms");

    private String pageInProperty;

    Pages(String pageInProperty) {
        this.pageInProperty = pageInProperty;
    }

    public String getPageInProperty() {
        return pageInProperty;
    }
}
