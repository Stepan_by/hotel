package by.epam.training.enums;

/**
 * Enum of roles
 */
public enum Roles {
    ADMIN, USER, GUEST
}
