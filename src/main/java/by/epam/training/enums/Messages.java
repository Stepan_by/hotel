package by.epam.training.enums;

/**
 * Enum for linking the messages
 */
public enum Messages {

    SUCCESS_BOOKING("user.booking.success.message"),

    //User
    PASSWORD_ERROR("user.passwordError"),
    LAST_PASSWORD_ERROR("user.lastPasswordError"),
    EMAIL_ERROR("user.emailError"),
    SUCCESS_PAYMENT("user.payment.success.message"),

    //Admin
    LESS_CAPACITY("admin.lessCapacity"),
    ROOM_ERROR("admin.room.error"),
    ROOM_DELETING_ERROR("admin.room.deleteError");

    private String messageInProperty;

    Messages(String messageInProperty) {
        this.messageInProperty = messageInProperty;
    }

    public String getMessageInProperty() {
        return messageInProperty;
    }
}
