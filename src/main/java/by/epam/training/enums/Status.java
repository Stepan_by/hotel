package by.epam.training.enums;

/**
 * Enum of reservation's status
 */
public enum Status {
    IN_PROCESS, CONFIRMED
}
