package by.epam.training.enums;

/**
 * Commands that the application can process
 */
public enum Commands {
    //User
    //User account
    LOGIN, LOGOUT, REGISTRATION, DELETE_ACCOUNT, CHANGE_SETTINGS, CHANGE_LANGUAGE,
    //User actions
    BOOK, BOOKINGS, CHANGE_PASSWORD,
    //Booking actions
    DELETE_BOOKING, CORRECT_BOOKING, PAY_INFO, PAY_BOOKING, UPDATE_BOOKING,

    //Admin
    //Bookings
    ADMIN_BOOKINGS, ADMIN_BOOK_INFO, UPDATE_ADMIN_BOOKINGS, ADMIN_DENY, ADMIN_CONFIRM,
    //Roles
    ADMIN_ROLES, UPDATE_ADMIN_ROLE, ADMIN_ROOMS,
    //Rooms
    ADMIN_ADD_ROOM, UPDATE_ADMIN_ROOM
}
