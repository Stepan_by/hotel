package by.epam.training.pool;

import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Class for getting the connection to the database.
 * Connection is getting from the pool which is located at the
 * Tomcat server and named 'HotelDataSource'.
 */
public class DBConnectionPool {

    private static final String DATA_SOURCE = "java:/comp/env/jdbc/HotelDataSource";
    private static DataSource source;
    private static final Logger LOGGER = Logger.getLogger(DBConnectionPool.class);

    static {
        try {
            InitialContext context = new InitialContext();
            source = (DataSource) context.lookup(DATA_SOURCE);
        } catch (NamingException e) {
            LOGGER.error("An exception in finding a db source : " + e);
        }
    }

    private DBConnectionPool() {
    }

    /**
     * Getting the connection to the database
     * @return connection
     */
    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = source.getConnection();
        } catch (SQLException e) {
            LOGGER.error("An exception in getting db connection : " + e);
        }
        return connection;
    }

}
