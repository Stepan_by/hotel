package by.epam.training.command.user.booking;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.reservation.ReservationDAO;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.Reservation;
import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static by.epam.training.enums.Pages.USER_BOOKINGS;
import static by.epam.training.enums.Status.CONFIRMED;
import static by.epam.training.enums.Status.IN_PROCESS;

/**
 * Command for showing all user's bookings
 */
public class UserBookingsCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;

    private static final Logger LOGGER = Logger.getLogger(UserBookingsCommand.class);

    //Pages
    private static final String BOOKINGS_PAGE = USER_BOOKINGS.getPageInProperty();

    //Attributes
    private static final String IN_PROCESS_ATTRIBUTE = "inProcess";
    private static final String CONFIRMED_ATTRIBUTE = "confirmed";
    private static final String USER = "user";

    public UserBookingsCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        ReservationDAO dao = new ReservationDAOImpl();
        User user = (User) request.getSession().getAttribute(USER);
        try {
            ArrayList<Reservation> reservations = dao.getAllUserReservations(user.getId());
            List<Reservation> list = reservations.stream().
                    filter(reservation -> reservation.getStatus() == IN_PROCESS).
                    collect(Collectors.toList());
            request.setAttribute(IN_PROCESS_ATTRIBUTE, list);
            request.setAttribute(CONFIRMED_ATTRIBUTE, reservations.stream().
                    filter(reservation -> reservation.getStatus() == CONFIRMED).
                    toArray());
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        request.getRequestDispatcher(PagesManager.getPage(BOOKINGS_PAGE)).forward(request, response);
    }
}
