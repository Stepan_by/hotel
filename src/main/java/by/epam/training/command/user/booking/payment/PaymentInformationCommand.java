package by.epam.training.command.user.booking.payment;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.dao.room.RoomDAOImpl;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.Reservation;
import by.epam.training.entities.Room;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static by.epam.training.enums.Pages.PAYMENT;

/**
 * Command for showing the paying page
 */
public class PaymentInformationCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;

    private static final Logger LOGGER = Logger.getLogger(PaymentInformationCommand.class);

    //Attributes
    public static final String ID = "id";
    public static final String ROOMS = "rooms";
    public static final String RESERVATION = "reservation";
    public static final String COST = "cost";

    private static final String PAYMENT_PAGE = PAYMENT.getPageInProperty();

    public PaymentInformationCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter(ID));
        try {
            addAttributes(id);
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        request.getRequestDispatcher(PagesManager.getPage(PAYMENT_PAGE)).forward(request, response);
    }

    /**
     * Add necessary attributes
     * @param id reservation id
     */
    private void addAttributes(int id) throws DAOException {
        Reservation reservation = new ReservationDAOImpl().getRecordByID(id);
        ArrayList<Room> rooms = new RoomDAOImpl().getRegisteredRoomsByReservation(reservation);
        request.setAttribute(RESERVATION, reservation);
        request.setAttribute(ROOMS, rooms);
        request.setAttribute(COST, rooms.stream().mapToInt(Room::getCost).sum());
    }
}
