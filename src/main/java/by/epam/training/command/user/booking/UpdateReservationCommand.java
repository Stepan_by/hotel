package by.epam.training.command.user.booking;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.reservation.ReservationDAO;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.entities.Reservation;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static by.epam.training.command.user.booking.BookingCommand.*;

/**
 * Command for correcting the user's booking
 */
public class UpdateReservationCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;

    public static final Logger LOGGER = Logger.getLogger(UpdateReservationCommand.class);
    public static final String DATA_FORMAT = "yyyy-mm-dd";

    //Attributes
    public static final String RESERVATION_ID = "reservationID";

    public UpdateReservationCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        try {
            updateReservationInDB();
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        new UserBookingsCommand(request, response).execute();
    }

    /**
     * Creating new reservation object and changing
     * the reservation in database to this
     */
    private void updateReservationInDB() throws DAOException {
        Reservation reservation = new Reservation();
        reservation.setId(Integer.parseInt(request.getParameter(RESERVATION_ID)));
        reservation.setType(request.getParameter(ROOM_TYPE));
        reservation.setNumberOfPersons(Integer.parseInt(request.getParameter(NUMBER_OF_PERSONS)));
        try {
            SimpleDateFormat format = new SimpleDateFormat(DATA_FORMAT);
            reservation.setDateFrom(format.parse(request.getParameter(DATE_IN)));
            reservation.setDateTo(format.parse(request.getParameter(DATE_OUT)));
        } catch (ParseException e) {
            LOGGER.error("An exception while parsing the date from request in updating the reservation " + reservation.getId());
        }
        ReservationDAO dao = new ReservationDAOImpl();
        dao.updateRecord(reservation);
    }
}
