package by.epam.training.command.user.booking;

import by.epam.training.command.Command;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.enums.Messages;
import by.epam.training.manager.MessageManager;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.Reservation;
import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static by.epam.training.enums.Pages.BOOKING;
import static by.epam.training.enums.Pages.SUCCESS;
import static by.epam.training.enums.Status.IN_PROCESS;

/**
 * Command for creating a user's booking
 */
public class BookingCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(BookingCommand.class);
    public static final String DATE_FORMAT = "yyyy-mm-dd";

    //Messages
    public String message = MessageManager.getMessage(Messages.SUCCESS_BOOKING.getMessageInProperty());

    //Pages
    public static final String BOOKING_PAGE = BOOKING.getPageInProperty();
    public static final String SUCCESS_PAGE = SUCCESS.getPageInProperty();

    //Attributes
    public static final String NUMBER_OF_PERSONS = "persons";
    public static final String ROOM_TYPE = "type";
    public static final String DATE_IN = "dayIn";
    public static final String DATE_OUT = "dayOut";
    public static final String USER = "user";
    public static final String MESSAGE = "message";

    public BookingCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        if (new ReservationDAOImpl().addRecord(createReservation())) {
            session.setAttribute(MESSAGE, message);
            response.sendRedirect(PagesManager.getPage(SUCCESS_PAGE));
        } else {
            response.sendRedirect(PagesManager.getPage(BOOKING_PAGE));
        }
    }

    /**
     * Creating the reservation from request's parameters
     * @return created reservation
     */
    private Reservation createReservation() {
        int persons = Integer.parseInt(request.getParameter(NUMBER_OF_PERSONS));
        String type = request.getParameter(ROOM_TYPE);
        Date dateIn = null;
        Date dateOut = null;
        try {
            dateIn = new SimpleDateFormat(DATE_FORMAT).parse(request.getParameter(DATE_IN));
            dateOut = new SimpleDateFormat(DATE_FORMAT).parse(request.getParameter(DATE_OUT));
        } catch (ParseException e) {
            LOGGER.error("An exception while getting date params from user : " + e);
        }
        User user = (User) request.getSession().getAttribute(USER);
        return new Reservation(dateIn, dateOut, user.getId(), type, IN_PROCESS, persons);
    }
}
