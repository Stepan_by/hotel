package by.epam.training.command.user.booking;

import by.epam.training.command.Command;
import by.epam.training.command.admin.bookings.ShowAllBookingsCommand;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.reservation.ReservationDAO;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for deleting the user's reservation
 */
public class DeleteBookingCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;

    private static final Logger LOGGER = Logger.getLogger(DeleteBookingCommand.class);

    //Attributes
    private static final String RESERVATION_ID = "id";
    private static final String USER = "user";

    public DeleteBookingCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        try {
            deleteRecordFromDB();
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        showPageAfterDeleting();
    }

    /**
     * Method executes the command to show the corresponding page
     * @throws ServletException
     * @throws IOException
     */
    private void showPageAfterDeleting() throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute(USER);
        switch (user.getRole()) {
            case USER:
                new UserBookingsCommand(request, response).execute();
                break;
            case ADMIN:
                new ShowAllBookingsCommand(request, response).execute();
                break;
            default:
                break;
        }
    }

    /**
     * Deleting the user's reservation from database
     */
    private void deleteRecordFromDB() throws DAOException {
        String reservationID = request.getParameter(RESERVATION_ID);
        ReservationDAO dao = new ReservationDAOImpl();
        dao.deleteRecordByID(Integer.parseInt(reservationID));
    }
}
