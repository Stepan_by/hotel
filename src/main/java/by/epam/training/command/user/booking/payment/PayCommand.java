package by.epam.training.command.user.booking.payment;

import by.epam.training.command.Command;
import by.epam.training.manager.MessageManager;
import by.epam.training.manager.PagesManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.training.enums.Messages.SUCCESS_PAYMENT;
import static by.epam.training.enums.Pages.SUCCESS;

/**
 * Command for paying for booking
 */
public class PayCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(PayCommand.class);

    //Attributes
    public static final String MESSAGE_ATTRIBUTE = "message";
    public static final String CARD = "credit-card";
    public static final String NUMBER = "number";
    public static final String OWNER = "owner";
    public static final String ID = "id";

    //Pages
    private static final String SUCCESS_PAGE = PagesManager.getPage(SUCCESS.getPageInProperty());

    //Messages
    private String message = MessageManager.getMessage(SUCCESS_PAYMENT.getMessageInProperty());

    public PayCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        updateDB();
        session.setAttribute(MESSAGE_ATTRIBUTE, message);
        response.sendRedirect(SUCCESS_PAGE);
    }

    /**
     * Here may be some actions related with
     * adding the card number to the database
     * or something else
     */
    private void updateDB() {
        String card = request.getParameter(CARD);
        String number = request.getParameter(NUMBER);
        String owner = request.getParameter(OWNER);
        int reservationId = Integer.parseInt(request.getParameter(ID));
        LOGGER.info("User paid for booking");
    }
}
