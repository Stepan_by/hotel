package by.epam.training.command.user.booking;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.Reservation;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.epam.training.command.user.booking.BookingCommand.DATE_IN;
import static by.epam.training.command.user.booking.BookingCommand.DATE_OUT;
import static by.epam.training.command.user.booking.BookingCommand.ROOM_TYPE;
import static by.epam.training.enums.Pages.BOOKING;

/**
 * Command for correcting the existing user's booking
 */
public class CorrectBookingCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;

    private static final Logger LOGGER = Logger.getLogger(CorrectBookingCommand.class);

    //Pages
    public static final String BOOKING_PAGE = BOOKING.getPageInProperty();

    //Attributes
    public static final String ID = "id";
    public static final String RESERVATION_ID = "reservationID";
    public static final String SELECTED = "selected";


    public CorrectBookingCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter(ID));
        try {
            Reservation reservation = new ReservationDAOImpl().getRecordByID(id);
            addAttributes(reservation);
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        request.getRequestDispatcher(PagesManager.getPage(BOOKING_PAGE)).forward(request, response);
    }

    /**
     * Adding attributes to request
     * @param reservation chosen reservation
     */
    private void addAttributes(Reservation reservation) {
        request.setAttribute(SELECTED, reservation.getNumberOfPersons());
        request.setAttribute(ROOM_TYPE, reservation.getType());
        request.setAttribute(DATE_IN, reservation.getDateFrom());
        request.setAttribute(DATE_OUT, reservation.getDateTo());
        request.setAttribute(RESERVATION_ID, reservation.getId());
    }
}
