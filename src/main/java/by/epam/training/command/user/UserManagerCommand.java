package by.epam.training.command.user;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.user.UserDAO;
import by.epam.training.dao.user.UserDAOImpl;
import by.epam.training.enums.Pages;
import by.epam.training.manager.MessageManager;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.training.enums.Messages.EMAIL_ERROR;

/**
 * Command for manipulating the user's settings
 */
public class UserManagerCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(UserManagerCommand.class);

    //Messages
    private String message = MessageManager.getMessage(EMAIL_ERROR.getMessageInProperty());

    //Pages
    private static final String SETTINGS = Pages.SETTINGS.getPageInProperty();

    //Attributes
    private static final String USER_NAME = "user-name";
    private static final String USER_EMAIL = "user-email";
    private static final String MESSAGE = "message";
    private static final String USER = "user";

    public UserManagerCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        User user = (User) session.getAttribute(USER);
        UserDAO userDAO = new UserDAOImpl();
        try {
            updateUserName(user, userDAO);
            updateUserEmail(user, userDAO);
            session.setAttribute(USER, user);
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        response.sendRedirect(PagesManager.getPage(SETTINGS));
    }

    /**
     * Update user's name in the database
     * @param user user whose name updating
     * @param userDAO data access object
     */
    private void updateUserName(User user, UserDAO userDAO) throws DAOException {
        String name = request.getParameter(USER_NAME);
        if (name.length() > 0) {
            userDAO.updateName(user.getEmail(), name);
            user.setName(name);
        }
    }

    /**
     * Update user's email in the database
     * if the email already exists then show the error message
     * @param user user whose email updating
     * @param userDAO data access object
     */
    private void updateUserEmail(User user, UserDAO userDAO) throws DAOException {
        String email = request.getParameter(USER_EMAIL);
        if (email.length() > 0) {
            if (!userDAO.updateEmail(user.getEmail(), email)) {
                session.setAttribute(MESSAGE, message);
            } else {
                user.setEmail(email);
            }
        }
    }
}
