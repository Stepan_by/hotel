package by.epam.training.command.factory;

import by.epam.training.command.Command;
import by.epam.training.command.FakeCommand;
import by.epam.training.command.account.*;
import by.epam.training.command.admin.bookings.*;
import by.epam.training.command.admin.roles.RolesCommand;
import by.epam.training.command.admin.roles.UpdateRole;
import by.epam.training.command.admin.rooms.AddingRoomCommand;
import by.epam.training.command.admin.rooms.RoomsCommand;
import by.epam.training.command.admin.rooms.UpdateRoomCommand;
import by.epam.training.command.user.booking.*;
import by.epam.training.command.user.booking.payment.PayCommand;
import by.epam.training.command.user.booking.payment.PaymentInformationCommand;
import by.epam.training.command.user.UserManagerCommand;
import by.epam.training.enums.Commands;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The simple factory for invoking the chosen command
 */
public class CommandFactory {

    private static final Logger LOGGER = Logger.getLogger(CommandFactory.class);

    public static final String COMMAND = "command";

    public static Command createCommand(HttpServletRequest request, HttpServletResponse response) {
        Commands command = Commands.valueOf(request.getParameter(COMMAND).toUpperCase());
        LOGGER.info("Command " + command + " invoked");
        switch (command) {
            //Account commands
            case LOGIN:
                return new LoginCommand(request, response);
            case LOGOUT:
                return new LogoutCommand(request, response);
            case REGISTRATION:
                return new RegistrationCommand(request, response);
            case DELETE_ACCOUNT:
                return new DeleteAccountCommand(request, response);

            //User's settings
            case CHANGE_SETTINGS:
                return new UserManagerCommand(request, response);
            case CHANGE_PASSWORD:
                return new ChangePasswordCommand(request, response);
            case CHANGE_LANGUAGE:
                return new ChangeLanguageCommand(request, response);

            //User's booking commands
            case BOOK:
                return new BookingCommand(request, response);
            case BOOKINGS:
                return new UserBookingsCommand(request, response);
            case DELETE_BOOKING:
                return new DeleteBookingCommand(request, response);
            case CORRECT_BOOKING:
                return new CorrectBookingCommand(request, response);
            case UPDATE_BOOKING:
                return new UpdateReservationCommand(request, response);
            //User's payment operations
            case PAY_INFO:
                return new PaymentInformationCommand(request, response);
            case PAY_BOOKING:
                return new PayCommand(request, response);

            //Admin commands
            //Admin bookings
            case ADMIN_BOOKINGS:
                return new ShowAllBookingsCommand(request, response);
            case ADMIN_BOOK_INFO:
                return new DetailsCommand(request, response);
            case UPDATE_ADMIN_BOOKINGS:
                return new UpdateAdminBookingsCommand(request, response);
            case ADMIN_DENY:
                return new DenyCommand(request, response);
            case ADMIN_CONFIRM:
                return new ConfirmCommand(request, response);
            //Actions with user's roles
            case ADMIN_ROLES:
                return new RolesCommand(request, response);
            case UPDATE_ADMIN_ROLE:
                return new UpdateRole(request, response);
            //Actions with rooms
            case ADMIN_ROOMS:
                return new RoomsCommand(request, response);
            case ADMIN_ADD_ROOM:
                return new AddingRoomCommand(request, response);
            case UPDATE_ADMIN_ROOM:
                return new UpdateRoomCommand(request, response);

            default:
                return new FakeCommand();
        }
    }
}
