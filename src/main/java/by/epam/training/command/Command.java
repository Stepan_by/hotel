package by.epam.training.command;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Interface for Command pattern
 */
public interface Command {

    /**
     * The main pattern's Command method
     * @throws ServletException
     * @throws IOException
     */
    void execute() throws ServletException, IOException;
}
