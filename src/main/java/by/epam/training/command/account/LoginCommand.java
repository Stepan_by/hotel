package by.epam.training.command.account;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.user.UserDAOImpl;
import by.epam.training.manager.MessageManager;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.training.enums.Messages.PASSWORD_ERROR;
import static by.epam.training.enums.Pages.LOGIN;
import static by.epam.training.enums.Pages.MAIN;

/**
 * Command for login the user.
 */
public class LoginCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);
    private static final int COOKIES_AGE = 1800;

    //Attributes
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String USER = "user";
    private static final String MESSAGE = "message";

    //Messages
    private String message = MessageManager.getMessage(PASSWORD_ERROR.getMessageInProperty());

    //Pages
    private static final String LOGIN_PAGE = LOGIN.getPageInProperty();
    private static final String MAIN_PAGE = MAIN.getPageInProperty();

    public LoginCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        User user = null;
        try {
            user = findUserInDB();
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        if (user != null) {
            session.setAttribute(USER, user);
            addLogPassCookies(user);
            response.sendRedirect(PagesManager.getPage(MAIN_PAGE));
            LOGGER.info(String.format("User id = %d logged in", user.getId()));
        } else {
            session.setAttribute(MESSAGE, message);
            response.sendRedirect(PagesManager.getPage(LOGIN_PAGE));
        }
    }

    /**
     * Adding last logged in email and password
     * @param user user's email and password
     */
    private void addLogPassCookies(User user) {
        response.addCookie(createCookie(EMAIL, user.getEmail()));
        response.addCookie(createCookie(PASSWORD, user.getPassword()));
    }

    /**
     * Creating the cookie with the path '/'
     * @param name cookie's name
     * @param value cookie's value
     * @return created cookie
     */
    private Cookie createCookie(String name, String value) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(COOKIES_AGE);
        cookie.setPath("/");
        return cookie;
    }

    /**
     * Take user from database by email and password
     * from request attributes.
     * @return user from database
     */
    private User findUserInDB() throws DAOException {
        String email = request.getParameter(EMAIL);
        String password = request.getParameter(PASSWORD);
        return new UserDAOImpl().getUserByLoginPassword(email, password);
    }
}
