package by.epam.training.command.account;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.user.UserDAOImpl;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.training.enums.Pages.MAIN;

/**
 * Command for deleting the user's account.
 */
public class DeleteAccountCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(DeleteAccountCommand.class);
    //Pages
    private static final String MAIN_PAGE = MAIN.getPageInProperty();

    //Attributes
    private static final String USER = "user";

    public DeleteAccountCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        try {
            deleteUserFromDB();
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        session.invalidate();
        response.sendRedirect(PagesManager.getPage(MAIN_PAGE));
    }

    /**
     * Delete User from database
     */
    private void deleteUserFromDB() throws DAOException {
        User user = (User) session.getAttribute(USER);
        removeAllCookies();
        new UserDAOImpl().delete(user.getId());
        LOGGER.info(String.format("User id = %d deleted", user.getId()));
    }

    /**
     * Remove all unnecessary cookies
     */
    private void removeAllCookies() {
        for (Cookie cookie : request.getCookies()) {
            cookie.setMaxAge(0);
            cookie.setValue(null);
            response.addCookie(cookie);
        }
    }
}
