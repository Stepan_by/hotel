package by.epam.training.command.account;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.user.UserDAOImpl;
import by.epam.training.manager.MessageManager;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.training.enums.Messages.LAST_PASSWORD_ERROR;
import static by.epam.training.enums.Pages.SETTINGS;

/**
 * Command for changing the user's password.
 */
public class ChangePasswordCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(ChangeLanguageCommand.class);

    //Messages
    private String message = MessageManager.getMessage(LAST_PASSWORD_ERROR.getMessageInProperty());

    //Pages
    private static final String SETTINGS_PAGE = SETTINGS.getPageInProperty();

    //Attributes
    private static final String OLD_PASSWORD = "user-old-password";
    private static final String NEW_PASSWORD = "user-password";
    private static final String MESSAGE = "message";
    private static final String USER = "user";

    public ChangePasswordCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        String password = request.getParameter(OLD_PASSWORD);
        User user = (User) session.getAttribute(USER);
        if (password.length() > 0) {
            if (isPasswordCorrect(user, password)) {
                try {
                    updatePasswordInDB(user);
                    session.setAttribute(USER, user);
                    LOGGER.info("User's password changed");
                } catch (DAOException e) {
                    LOGGER.error(e);
                }
            } else {
                session.setAttribute(MESSAGE, message);
                LOGGER.info("User typed his password incorrectly");
            }
        }
        response.sendRedirect(PagesManager.getPage(SETTINGS_PAGE));
    }

    /**
     * Update user's password in database.
     * @param user current user
     */
    private void updatePasswordInDB(User user) throws DAOException {
        String newPassword = request.getParameter(NEW_PASSWORD);
        new UserDAOImpl().updatePassword(user, newPassword);
    }

    /**
     * Checking the user's password.
     * @param user current user
     * @param password entered password
     * @return true if passwords equals and false otherwise
     */
    private boolean isPasswordCorrect(User user, String password) {
        return user.getPassword().equals(password);
    }
}
