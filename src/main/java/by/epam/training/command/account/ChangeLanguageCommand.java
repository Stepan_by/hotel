package by.epam.training.command.account;

import by.epam.training.command.Command;
import by.epam.training.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

/**
 * Command for changing application's language.
 */
public class ChangeLanguageCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;

    private static final Logger LOGGER = Logger.getLogger(ChangeLanguageCommand.class);

    //Attributes
    private static final String LANGUAGE = "language";
    private static final String LOCALE = "locale";
    private static final String REFERER = "Referer";

    public ChangeLanguageCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        String locale = request.getParameter(LANGUAGE);
        changeSessionLocale(locale);
        changeMessageManagerLocale(locale);
        response.sendRedirect(request.getHeader(REFERER));
        LOGGER.info("The application language changed to " + locale);
    }

    /**
     * Setting new locale in the attribute LOCALE
     * in session
     * @param locale new locale
     */
    private void changeSessionLocale(String locale) {
        request.getSession().setAttribute(LOCALE, locale);
    }

    /**
     * Setting new locale in message manager
     * @param locale new locale in which the first two
     * symbols - language, after that the symbol '_' and
     * the final part in two symbols - country.
     */
    private void changeMessageManagerLocale(String locale) {
        String language = locale.substring(0, 2);
        String country = locale.substring(3, 5);
        MessageManager.changeLocale(new Locale(language, country));
    }
}
