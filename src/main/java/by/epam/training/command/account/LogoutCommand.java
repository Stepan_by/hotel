package by.epam.training.command.account;

import by.epam.training.command.Command;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.training.enums.Pages.MAIN;

/**
 * Command for logging out.
 */
public class LogoutCommand implements Command {

    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(LogoutCommand.class);

    //Pages
    private static final String MAIN_PAGE = MAIN.getPageInProperty();

    //Attributes
    public static final String USER = "user";

    public LogoutCommand(HttpServletRequest request, HttpServletResponse response) {
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        User user = (User) session.getAttribute(USER);
        LOGGER.info(String.format("User id = %d logged out", user.getId()));
        session.invalidate();
        response.sendRedirect(PagesManager.getPage(MAIN_PAGE));
    }
}
