package by.epam.training.command.account;

import by.epam.training.command.Command;
import by.epam.training.dao.user.UserDAO;
import by.epam.training.dao.user.UserDAOImpl;
import by.epam.training.enums.Roles;
import by.epam.training.manager.MessageManager;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.training.enums.Messages.EMAIL_ERROR;
import static by.epam.training.enums.Pages.MAIN;
import static by.epam.training.enums.Pages.REGISTRATION;

/**
 * Command for user's registration
 */
public class RegistrationCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    //Pages
    private static final String REGISTRATION_PAGE = REGISTRATION.getPageInProperty();
    private static final String MAIN_PAGE = MAIN.getPageInProperty();

    //Attributes
    private static final String EMAIL = "email";
    private static final String NAME = "name";
    private static final String PASSWORD = "password";
    private static final String USER = "user";
    private static final String MESSAGE = "message";

    //Message
    private String message = MessageManager.getMessage(EMAIL_ERROR.getMessageInProperty());

    public RegistrationCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        User user = createUser();
        if (addUserToDB(user)) {
            session.setAttribute(USER, user);
            response.sendRedirect(PagesManager.getPage(MAIN_PAGE));
        } else {
            session.setAttribute(MESSAGE, message);
            response.sendRedirect(PagesManager.getPage(REGISTRATION_PAGE));
        }
    }

    /**
     * Creating the user's object
     * @return {@code User} object
     */
    private User createUser() {
        String name = request.getParameter(NAME);
        String email = request.getParameter(EMAIL);
        String password = request.getParameter(PASSWORD);
        return new User(name, email, password, Roles.USER);
    }

    /**
     * Adding the user to database
     * @param user user that we are adding
     * @return true if the user added, false otherwise
     */
    private boolean addUserToDB(User user) {
        UserDAO userDAO = new UserDAOImpl();
        return userDAO.save(user);
    }
}
