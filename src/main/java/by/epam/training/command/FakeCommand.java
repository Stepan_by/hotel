package by.epam.training.command;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Class that implements empty command
 */
public class FakeCommand implements Command {
    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        //Empty command
    }
}
