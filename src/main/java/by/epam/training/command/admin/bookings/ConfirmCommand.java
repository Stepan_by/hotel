package by.epam.training.command.admin.bookings;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.dao.room.RoomDAO;
import by.epam.training.dao.room.RoomDAOImpl;
import by.epam.training.manager.MessageManager;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.Reservation;
import by.epam.training.entities.Room;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

import static by.epam.training.command.admin.bookings.DetailsCommand.*;
import static by.epam.training.command.admin.bookings.ShowAllBookingsCommand.CONFIRMED;
import static by.epam.training.command.admin.bookings.ShowAllBookingsCommand.IN_PROCESS;
import static by.epam.training.enums.Messages.LESS_CAPACITY;
import static by.epam.training.enums.Pages.BOOK_INFO;

/**
 * Command for confirming the booking
 */
public class ConfirmCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(ConfirmCommand.class);

    //Pages
    private static final String BOOK_INFO_PAGE = BOOK_INFO.getPageInProperty();

    //Messages
    private String message = MessageManager.getMessage(LESS_CAPACITY.getMessageInProperty());

    //Attributes
    private static final String MESSAGE_ATTRIBUTE = "message";

    public ConfirmCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @Override
    public void execute() throws ServletException, IOException {
        ArrayList<Room> rooms = (ArrayList<Room>) session.getAttribute(CHOSEN);
        Reservation reservation = (Reservation) session.getAttribute(RESERVATION);
        int totalChosenRoomsCapacity = rooms.stream().mapToInt(Room::getCapacity).sum();
        if (reservation.getNumberOfPersons() <= totalChosenRoomsCapacity) {
            try {
                updateDB(reservation, rooms);
                removeSessionAttributes();
                LOGGER.info("The booking " + reservation.getId() + " confirmed");
            } catch (DAOException e) {
                LOGGER.error(e);
            }
            new ShowAllBookingsCommand(request, response).execute();
        } else {
            session.setAttribute(MESSAGE_ATTRIBUTE, message);
            response.sendRedirect(PagesManager.getPage(BOOK_INFO_PAGE));
        }
    }

    /**
     * Remove all unnecessary attributes
     */
    public void removeSessionAttributes() {
        session.removeAttribute(AVAILABLE);
        session.removeAttribute(CHOSEN);
        session.removeAttribute(USER);
        session.removeAttribute(RESERVATION);
        session.removeAttribute(CAPACITY);
        session.removeAttribute(COST);
        session.removeAttribute(CONFIRMED);
        session.removeAttribute(IN_PROCESS);
    }

    /**
     * Changing the reservation status.
     * Booking the chosen rooms
     * @param reservation which we are managing
     * @param rooms that are booking
     */
    private void updateDB(Reservation reservation, ArrayList<Room> rooms) throws DAOException {
        RoomDAO roomDAO = new RoomDAOImpl();
        new ReservationDAOImpl().changeRecordStatus(reservation);
        roomDAO.freeRoomsByReservationId(reservation.getId());
        roomDAO.registrationRooms(rooms, reservation.getId());
    }
}
