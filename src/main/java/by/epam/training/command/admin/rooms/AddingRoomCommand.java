package by.epam.training.command.admin.rooms;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.room.RoomDAOImpl;
import by.epam.training.enums.Pages;
import by.epam.training.manager.MessageManager;
import by.epam.training.manager.PagesManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.training.enums.Messages.ROOM_ERROR;

/**
 * Class for adding a new room in the database
 */
public class AddingRoomCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(AddingRoomCommand.class);

    //Attributes
    public static final String NUMBER = "number";
    public static final String TYPE = "type";
    public static final String MESSAGE = "message";

    //Pages
    public static final String ROOMS = PagesManager.getPage(Pages.ROOMS.getPageInProperty());

    //Messages
    public String ERROR_MESSAGE = MessageManager.getMessage(ROOM_ERROR.getMessageInProperty());

    public AddingRoomCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        try {
            updateDB();
        } catch (DAOException e) {
            LOGGER.error(e);
            session.setAttribute(MESSAGE, ERROR_MESSAGE);
        }
        response.sendRedirect(ROOMS);
    }

    /**
     * Adding the room to the database
     */
    private void updateDB() throws DAOException {
        int number = Integer.parseInt(request.getParameter(NUMBER));
        String type = request.getParameter(TYPE);
        new RoomDAOImpl().addRoom(number, type);
        LOGGER.info("New room with number " + number + " and type " + type + " added to the database");
    }
}
