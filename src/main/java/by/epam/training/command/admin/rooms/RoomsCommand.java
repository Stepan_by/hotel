package by.epam.training.command.admin.rooms;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.room.RoomDAOImpl;
import by.epam.training.enums.Pages;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.Room;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Command for changing the rooms in the hotel
 */
public class RoomsCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(RoomsCommand.class);
    private static final int RECORDS_PER_PAGE = 5;

    //Pages
    public static final String ROOMS = PagesManager.getPage(Pages.ROOMS.getPageInProperty());

    //Attributes
    public static final String ROOMS_ATTRIBUTE = "rooms";
    public static final String PAGE = "page";
    public static final String PAGES_NUMBER = "pages";
    public static final String CURRENT_PAGE = "curPage";

    public RoomsCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        int page = 1;
        if (request.getParameter(PAGE) != null) {
            page = Integer.parseInt(request.getParameter(PAGE));
        }
        try {
            ArrayList<Room> rooms = new RoomDAOImpl().getAllRooms( (page - 1) * RECORDS_PER_PAGE, RECORDS_PER_PAGE);
            session.setAttribute(ROOMS_ATTRIBUTE, rooms);
            session.setAttribute(PAGES_NUMBER, countPagesNumber(new RoomDAOImpl().getNumberOfRooms()));
            session.setAttribute(CURRENT_PAGE, page);
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        response.sendRedirect(ROOMS);
    }

    /**
     * Get the number of the pages
     * @param size number of the rooms
     * @return number of the pages
     */
    private int countPagesNumber(double size) {
        return size % RECORDS_PER_PAGE > 0 ? (int) size / RECORDS_PER_PAGE + 1 : (int) size / RECORDS_PER_PAGE;
    }
}
