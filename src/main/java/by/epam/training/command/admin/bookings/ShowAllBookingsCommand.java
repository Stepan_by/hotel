package by.epam.training.command.admin.bookings;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.enums.Status;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.Reservation;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

import static by.epam.training.enums.Pages.ADMIN_BOOKINGS;

/**
 * Command for showing all booking in the hotel
 */
public class ShowAllBookingsCommand implements Command {

    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(ShowAllBookingsCommand.class);

    //Pages
    private static final String BOOKINGS_ADMIN_PAGE = ADMIN_BOOKINGS.getPageInProperty();

    //Messages
    public static final String IN_PROCESS = "inProcess";
    public static final String CONFIRMED = "confirmed";

    public ShowAllBookingsCommand(HttpServletRequest request, HttpServletResponse response) {
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        try {
            ArrayList<Reservation> reservations = new ReservationDAOImpl().getAllReservations();
            session.setAttribute(IN_PROCESS, reservations.stream()
                    .filter(reservation -> reservation.getStatus() == Status.IN_PROCESS)
                    .collect(Collectors.toList()));
            session.setAttribute(CONFIRMED, reservations.stream()
                    .filter(reservation -> reservation.getStatus() == Status.CONFIRMED)
                    .collect(Collectors.toList()));
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        response.sendRedirect(PagesManager.getPage(BOOKINGS_ADMIN_PAGE));
    }
}
