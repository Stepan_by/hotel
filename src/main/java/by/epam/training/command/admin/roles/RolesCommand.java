package by.epam.training.command.admin.roles;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.user.UserDAO;
import by.epam.training.dao.user.UserDAOImpl;
import by.epam.training.manager.PagesManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.training.enums.Pages.LOGIN;
import static by.epam.training.enums.Pages.ROLES;

/**
 * Command for changing the user's roles
 */
public class RolesCommand implements Command {

    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(RolesCommand.class);

    //Attributes
    public static final String USERS = "users";

    //Pages
    public static final String ROLES_PAGE = PagesManager.getPage(ROLES.getPageInProperty());

    public RolesCommand(HttpServletRequest request, HttpServletResponse response) {
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        try {
            session.setAttribute(USERS, new UserDAOImpl().getAllUsers());
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        response.sendRedirect(ROLES_PAGE);
    }
}
