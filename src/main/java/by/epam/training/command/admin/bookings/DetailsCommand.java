package by.epam.training.command.admin.bookings;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.dao.room.RoomDAO;
import by.epam.training.dao.room.RoomDAOImpl;
import by.epam.training.dao.user.UserDAOImpl;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.Reservation;
import by.epam.training.entities.Room;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

import static by.epam.training.enums.Pages.BOOK_INFO;

/**
 * Command for showing the full information about the booking
 */
public class DetailsCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(DetailsCommand.class);

    //Pages
    private static final String BOOKING_INFORMATION = BOOK_INFO.getPageInProperty();

    //Attributes
    public static final String ID = "id";
    public static final String COST = "cost";
    public static final String CAPACITY = "capacity";
    public static final String RESERVATION = "reservation";
    public static final String AVAILABLE = "available";
    public static final String CHOSEN = "chosen";
    public static final String USER = "reservationUser";

    public DetailsCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        RoomDAO roomDAO = new RoomDAOImpl();
        Reservation reservation = null;
        try {
            reservation = findReservationInDB();
            session.setAttribute(USER, new UserDAOImpl().getUserByID(reservation.getUserId()));
            addAvailableRoomsAsAttribute(roomDAO, reservation);
            addChosenRoomsAsAttribute(roomDAO, reservation);
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        session.setAttribute(RESERVATION, reservation);
        request.getRequestDispatcher(PagesManager.getPage(BOOKING_INFORMATION)).forward(request, response);
    }

    /**
     * Find the reservation by attribute id from the request
     * @return reservation from database
     */
    private Reservation findReservationInDB() throws DAOException {
        int id = Integer.parseInt(request.getParameter(ID));
        return new ReservationDAOImpl().getRecordByID(id);
    }

    /**
     * Take the available rooms from database and add them
     * to the request as an attribute
     * @param roomDAO data access object for rooms in the hotel
     * @param reservation the reservation for getting the demand period
     */
    private void addAvailableRoomsAsAttribute(RoomDAO roomDAO, Reservation reservation) throws DAOException {
        ArrayList<Room> availableRooms = roomDAO.getAvailableRoomsByType(
                reservation.getType(), reservation.getDateFrom(), reservation.getDateTo());
        session.setAttribute(AVAILABLE, availableRooms);
    }

    /**
     * Take the rooms chosen before from database and add them
     * to the request as an attribute
     * @param roomDAO data access object for rooms in the hotel
     * @param reservation the reservation for getting the demand period
     */
    private void addChosenRoomsAsAttribute(RoomDAO roomDAO, Reservation reservation) throws DAOException {
        ArrayList<Room> chosenRooms = roomDAO.getRegisteredRoomsByReservation(reservation);
        session.setAttribute(CHOSEN, chosenRooms);
        session.setAttribute(COST, chosenRooms.stream().mapToInt(Room::getCost).sum());
        session.setAttribute(CAPACITY, chosenRooms.stream().mapToInt(Room::getCapacity).sum());
    }
}
