package by.epam.training.command.admin.rooms;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.room.RoomDAOImpl;
import by.epam.training.enums.Messages;
import by.epam.training.enums.Pages;
import by.epam.training.manager.MessageManager;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.Room;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

import static by.epam.training.enums.Messages.*;

/**
 * Command for updating the room in the database
 */
public class UpdateRoomCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(UpdateRoomCommand.class);

    //Attributes
    public static final String ROOM = "room";
    public static final String TYPE = "type";
    public static final String MESSAGE = "message";

    //Pages
    public static final String ROOMS = PagesManager.getPage(Pages.ROOMS.getPageInProperty());

    public UpdateRoomCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @Override
    public void execute() throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter(ROOM));
        String type = request.getParameter(TYPE);
        ArrayList<Room> rooms = (ArrayList<Room>) session.getAttribute(RoomsCommand.ROOMS_ATTRIBUTE);
        try {
            if (type.length() == 0) {
                deleteRoom(id, rooms);
            } else {
                updateRoom(id, type, rooms);
                LOGGER.info("Room with id = " + id + " updated and its type now " + type);
            }
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        response.sendRedirect(ROOMS);
    }

    /**
     * Delete the room from the database and update session
     * @param id room's id
     * @param rooms session's rooms
     */
    private void deleteRoom(int id, ArrayList<Room> rooms) {
        if (new RoomDAOImpl().deleteRoom(id)) {
            session.setAttribute(RoomsCommand.ROOMS_ATTRIBUTE,
                    rooms.stream().filter(room -> room.getId() != id).collect(Collectors.toList()));
            LOGGER.info("Room with id = " + id + " deleted");
        } else {
            session.setAttribute(MESSAGE, MessageManager.getMessage(ROOM_DELETING_ERROR.getMessageInProperty()));
        }
    }

    /**
     * Update the room's type and update session
     * @param id room's id
     * @param type new room's type
     * @param rooms session's rooms
     */
    private void updateRoom(int id, String type, ArrayList<Room> rooms) throws DAOException {
        new RoomDAOImpl().updateRoom(id, type);
        rooms.stream().filter(room -> room.getId() == id).forEach(room1 -> room1.setType(type));
        session.setAttribute(RoomsCommand.ROOMS_ATTRIBUTE, rooms);
    }
}
