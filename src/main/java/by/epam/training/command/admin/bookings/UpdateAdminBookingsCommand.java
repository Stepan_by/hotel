package by.epam.training.command.admin.bookings;

import by.epam.training.command.Command;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

import static by.epam.training.command.admin.bookings.DetailsCommand.*;
import static by.epam.training.enums.Pages.BOOK_INFO;

/**
 * Command for updating the details of the booking
 * after clicking the plus or minus buttons in the
 * available or chosen rooms
 */
public class UpdateAdminBookingsCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    //Pages
    private static final String BOOKING_INFORMATION = BOOK_INFO.getPageInProperty();

    //Attributes
    private static final String ACTION = "action";
    private static final String ROOM = "room";
    private static final String ADD = "add";
    private static final String SUB = "sub";

    public UpdateAdminBookingsCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @Override
    public void execute() throws ServletException, IOException {
        Integer roomNumber = Integer.parseInt(request.getParameter(ROOM));
        ArrayList<Room> availableRooms = (ArrayList<Room>) session.getAttribute(AVAILABLE);
        ArrayList<Room> chosenRooms = (ArrayList<Room>) session.getAttribute(CHOSEN);
        switch (request.getParameter(ACTION)) {
            case ADD:
                passRoom(availableRooms, chosenRooms, roomNumber);
                break;
            case SUB:
                passRoom(chosenRooms, availableRooms, roomNumber);
                break;
            default:
                break;
        }
        session.setAttribute(AVAILABLE, availableRooms);
        session.setAttribute(CHOSEN, chosenRooms);
        session.setAttribute(COST, chosenRooms.stream().mapToInt(Room::getCost).sum());
        session.setAttribute(CAPACITY, chosenRooms.stream().mapToInt(Room::getCapacity).sum());
        request.getRequestDispatcher(PagesManager.getPage(BOOKING_INFORMATION)).forward(request, response);
    }

    /**
     * The method is for passing the room from available rooms
     * to chosen and in the opposite direction
     * @param from the list where get the room
     * @param to the list where add the room
     * @param number number of the room
     */
    private void passRoom(ArrayList<Room> from, ArrayList<Room> to, int number) {
        Room room = from.stream().filter(r -> r.getNumber() == number).findFirst().get();
        to.add(room);
        from.remove(room);
    }
}
