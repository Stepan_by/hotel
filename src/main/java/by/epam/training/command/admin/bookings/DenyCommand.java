package by.epam.training.command.admin.bookings;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.entities.Reservation;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.training.command.admin.bookings.DetailsCommand.*;
import static by.epam.training.command.admin.bookings.ShowAllBookingsCommand.CONFIRMED;
import static by.epam.training.command.admin.bookings.ShowAllBookingsCommand.IN_PROCESS;

/**
 * Command for denying
 */
public class DenyCommand implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    private static final Logger LOGGER = Logger.getLogger(DenyCommand.class);

    public DenyCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        Reservation reservation = (Reservation) session.getAttribute(RESERVATION);
        try {
            new ReservationDAOImpl().deleteRecordByID(reservation.getId());
            removeSessionAttributes();
            LOGGER.info("The booking " + reservation.getId() + " denied");
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        new ShowAllBookingsCommand(request, response).execute();
    }

    /**
     * Remove all unnecessary attributes
     */
    public void removeSessionAttributes() {
        session.removeAttribute(AVAILABLE);
        session.removeAttribute(CHOSEN);
        session.removeAttribute(USER);
        session.removeAttribute(RESERVATION);
        session.removeAttribute(CONFIRMED);
        session.removeAttribute(IN_PROCESS);
    }
}
