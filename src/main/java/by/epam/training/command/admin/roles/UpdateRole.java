package by.epam.training.command.admin.roles;

import by.epam.training.command.Command;
import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.user.UserDAOImpl;
import by.epam.training.enums.Roles;
import by.epam.training.manager.PagesManager;
import by.epam.training.entities.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

import static by.epam.training.command.admin.roles.RolesCommand.USERS;
import static by.epam.training.enums.Pages.ROLES;

/**
 * Command for updating the user's role
 */
public class UpdateRole implements Command {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;


    private static final Logger LOGGER = Logger.getLogger(UpdateRole.class);

    //Attributes
    public static final String ROLE = "role";
    public static final String USER = "user";

    //Pages
    public static final String ROLES_PAGE = PagesManager.getPage(ROLES.getPageInProperty());

    public UpdateRole(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * Command
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute() throws ServletException, IOException {
        changeUserRole();
        response.sendRedirect(ROLES_PAGE);
    }

    /**
     * Changing the user's role
     */
    private void changeUserRole() {
        int id = Integer.parseInt(request.getParameter(USER));
        Roles role = Roles.valueOf(request.getParameter(ROLE).toUpperCase());
        try {
            updateUserInDB(id, role);
            updateSessionUser(id, role);
            LOGGER.info("User's id = " + id + " role changed to " + role);
        } catch (DAOException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Update the user's role in the database
     * @param id user's ID
     * @param role new user's role
     */
    private void updateUserInDB(int id, Roles role) throws DAOException {
        new UserDAOImpl().changeRoleByUserID(id, role);
    }

    /**
     * Update user in the session
     * @param id user's ID
     * @param role new user's role
     */
    @SuppressWarnings("unchecked")
    private void updateSessionUser(int id, Roles role) {
        ArrayList<User> users = (ArrayList<User>) session.getAttribute(USERS);
        users.stream().filter(user -> user.getId() == id).forEach(user1 -> user1.setRole(role));
        session.setAttribute(USERS, users);
    }
}
