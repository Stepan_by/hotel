package by.epam.training.dao.room;

import by.epam.training.dao.exception.DAOException;
import by.epam.training.entities.Reservation;
import by.epam.training.entities.Room;

import java.util.ArrayList;
import java.util.Date;

/**
 * Interface for DAO pattern for rooms
 */
public interface RoomDAO {

    /**
     * Getting all available rooms by chosen type in the chosen period
     * @param type chosen type
     * @param dateFrom date from
     * @param dateTill date till
     * @return rooms from database
     */
    ArrayList<Room> getAvailableRoomsByType(String type, Date dateFrom, Date dateTill) throws DAOException;

    /**
     * Booking the rooms in the hotel
     * @param rooms rooms which we are booking
     * @param reservationId reservation which contains these rooms
     */
    void registrationRooms(ArrayList<Room> rooms, int reservationId) throws DAOException;

    /**
     * Free booked rooms
     * @param reservationId reservation which contained these rooms
     */
    void freeRoomsByReservationId(int reservationId) throws DAOException;

    /**
     * Getting all rooms by given reservation
     * @param reservation reservation which is given
     * @return rooms by reservation
     */
    ArrayList<Room> getRegisteredRoomsByReservation(Reservation reservation) throws DAOException;

    /**
     * Add room to the database
     * @param number the room's number
     * @param type the room's type
     * @return added room or null if it wasn't added
     */
    Room addRoom(int number, String type) throws DAOException;

    /**
     * Getting all rooms from the database
     * @return rooms
     */
    ArrayList<Room> getAllRooms() throws DAOException;

    /**
     * Delete the room by its ID
     * @param id room's id
     */
    boolean deleteRoom(int id);

    /**
     * Update the room's type
     * @param id room's id
     * @param type new room's type
     */
    void updateRoom(int id, String type) throws DAOException;

    /**
     * Getting the part of rooms starts with {@param offset} to {@param offset} + {@param noOfRecords}
     * @param offset the number of the record with which we started
     * @param noOfRecords number of the records we are getting
     * @return rooms
     * @throws DAOException
     */
    ArrayList<Room> getAllRooms(int offset, int noOfRecords) throws DAOException;

    /**
     * Getting the number of the rooms in the database
     * @return number of the rooms
     * @throws DAOException
     */
    int getNumberOfRooms() throws DAOException;
}
