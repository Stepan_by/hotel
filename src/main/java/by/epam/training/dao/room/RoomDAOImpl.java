package by.epam.training.dao.room;

import by.epam.training.dao.exception.DAOException;
import by.epam.training.entities.Reservation;
import by.epam.training.entities.Room;
import by.epam.training.pool.DBConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Implementation of DAO pattern for rooms
 */
public class RoomDAOImpl implements RoomDAO {

    private static final Logger LOGGER = Logger.getLogger(RoomDAOImpl.class);

    //Attributes
    public static final String ID = "id";
    public static final String NUMBER = "number";
    public static final String TYPE = "type";
    public static final String COST = "cost";
    public static final String CAPACITY = "capacity";
    public static final String DATE_IN = "date_in";
    public static final String DATE_OUT = "date_out";

    /**
     * Query for selecting the rooms by the given reservation ID
     */
    private static final String SELECT_REGISTERED_ROOMS_BY_RESERVATION_ID =
            "SELECT * FROM room " +
            "JOIN reservation_room ON room.id = reservation_room.id_room " +
            "WHERE id_reservation = ?";

    /**
     * Query for booking the rooms in the database.
     * It means we are setting the value to the column reservation_id for each booking room
     */
    private static final String REGISTERING_ROOMS =
            "INSERT INTO reservation_room VALUES (?, ?);";

    /**
     * Query for freeing the rooms and making them available for booking
     */
    private static final String UNREGISTERED_ROOMS =
            "DELETE FROM reservation_room WHERE id_reservation = ?;";

    /**
     * Query for selecting the rooms by the given type
     */
    private static final String SELECT_ROOMS_ID_BY_TYPE =
            "SELECT room.id FROM room " +
            "JOIN room_type ON room.type = room_type.id " +
            "WHERE room_type.name = ?;";


    private static final String SELECT_RESERVED_DATES =
            "SELECT reservation.date_in, reservation.date_out FROM reservation " +
            "JOIN reservation_room ON reservation.id = reservation_room.id_reservation WHERE id_room = ?;";

    /**
     * Query for getting the information about the room's type
     */
    private static final String GET_ROOM_TYPE_PARAMS =
            "SELECT id, name, cost, max_capacity AS capacity FROM room_type WHERE room_type.name = ?;";

    /**
     * Query for selecting the room by its number
     */
    private static final String SELECT_ROOM_BY_ID =
            "SELECT room.*, room_type.cost AS cost, room_type.max_capacity AS capacity FROM room " +
            "JOIN room_type ON room.type = room_type.id " +
            "WHERE room.id = ?;";

    /**
     * Query for selecting all rooms from the database
     */
    private static final String SELECT_ALL_ROOMS =
            "SELECT room.id, room.number, room_type.name AS type FROM room " +
            "JOIN room_type ON room.type = room_type.id ORDER BY number;";

    /**
     * Query for adding the room to the database
     */
    private static final String ADD_ROOM =
            "INSERT INTO room " +
            "VALUES (0, ?, (SELECT room_type.id FROM room_type WHERE room_type.name = ?));";

    /**
     * Query for deleting the room
     */
    private static final String DELETE_ROOM = "DELETE FROM room WHERE room.id = ?;";

    /**
     * Query for updating the room's type
     */
    private static final String UPDATE_ROOM =
            "UPDATE room SET room.type = " +
            "(SELECT room_type.id FROM room_type WHERE room_type.name = ?) WHERE room.id = ?;";

    /**
     * Query for getting the rooms starts wit the offset
     */
    private static final String GET_ROOMS_WITH_OFFSET =
            "SELECT room.id, room.number, room_type.name AS type FROM room " +
            "JOIN room_type ON room.type = room_type.id ORDER BY number LIMIT ?, ?;";

    /**
     * Getting all available rooms by the type
     * @param type chosen type
     * @param dateFrom date from
     * @param dateTill date till
     * @return rooms
     */
    @Override
    public ArrayList<Room> getAvailableRoomsByType(String type, Date dateFrom, Date dateTill) throws DAOException {
        ArrayList<Integer> roomsId;
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_RESERVED_DATES)) {
            roomsId = selectRoomsIDByType(connection, type);
            Iterator<Integer> iterator = roomsId.iterator();
            removeBookedRooms(iterator, preparedStatement, dateFrom, dateTill);
        } catch (SQLException e) {
            throw new DAOException("Exception while selection all rooms " + e);
        }
        return selectRoomsByIDs(roomsId);
    }

    /**
     * Check the list of the rooms and remove those who is booked
     * at the given period
     * @param iterator iterator of the list of the rooms
     * @param statement where we are getting information
     * @param dateFrom date since user want to book a room
     * @param dateTill date till user want ot book a room
     * @throws SQLException
     */
    private void removeBookedRooms(Iterator<Integer> iterator, PreparedStatement statement,
                                   Date dateFrom, Date dateTill) throws SQLException, DAOException {
        ResultSet resultSet = null;
        while (iterator.hasNext()){
            int number = iterator.next();
            statement.setInt(1, number);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Date dateIn = resultSet.getDate(DATE_IN);
                Date dateOut = resultSet.getDate(DATE_OUT);
                if (!(dateTill.before(dateIn) || dateFrom.after(dateOut))) {
                    iterator.remove();
                    break;
                }
            }
        }
        closeResultSet(resultSet);
    }

    /**
     * Selecting the rooms from database by its IDs
     * @param ids array list of ID
     * @return rooms
     */
    private ArrayList<Room> selectRoomsByIDs(ArrayList<Integer> ids) throws DAOException {
        ArrayList<Room> rooms = new ArrayList<>();
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ROOM_BY_ID)) {
            for (Integer id : ids) {
                preparedStatement.setInt(1, id);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    rooms.add(new Room(id, resultSet.getInt(NUMBER), resultSet.getInt(COST), resultSet.getInt(CAPACITY)));
                }
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            throw new DAOException("An exception while selection rooms by its ID " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return rooms;
    }

    /**
     * Registration rooms to the given reservation
     * @param rooms rooms which we are booking
     * @param reservationId reservation which contains these rooms
     */
    @Override
    public void registrationRooms(ArrayList<Room> rooms, int reservationId) throws DAOException {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(REGISTERING_ROOMS)) {
            connection.setAutoCommit(false);
            for (Room room : rooms) {
                statement.setInt(1, reservationId);
                statement.setInt(2, room.getId());
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        } catch (SQLException e) {
            throw new DAOException("An exception while registered rooms " + e);
        }
    }

    /**
     * Releasing the room from the given reservation
     * @param reservationId reservation which contained these rooms
     */
    @Override
    public void freeRoomsByReservationId(int reservationId) throws DAOException {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UNREGISTERED_ROOMS)) {
            preparedStatement.setInt(1, reservationId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("An exception in freeing rooms " + e);
        }
    }

    /**
     * Getting the rooms which is belonging to the given reservation
     * @param reservation reservation which is given
     * @return rooms
     */
    @Override
    public ArrayList<Room> getRegisteredRoomsByReservation(Reservation reservation) throws DAOException {
        ArrayList<Room> rooms;
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_REGISTERED_ROOMS_BY_RESERVATION_ID)) {
            preparedStatement.setInt(1, reservation.getId());
            rooms = selectRoomsWithFullInfo(connection, preparedStatement, reservation.getType());
        } catch (SQLException e) {
            throw new DAOException("An exception while getting user's rooms " + e);
        }
        return rooms;
    }

    /**
     * Adding the room to the database
     * @param number the room's number
     * @param type the room's type
     * @return {@code Room} object that was added or null
     * if the room wasn't added
     */
    @Override
    public Room addRoom(int number, String type) throws DAOException {
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ADD_ROOM, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, number);
            preparedStatement.setString(2, type);
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                return new Room(resultSet.getInt(1), number, type);
            }
        } catch (SQLException e) {
            throw new DAOException("An exception while adding the room to the database " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return null;
    }

    /**
     * Getting all room from the database
     * @return rooms
     */
    @Override
    public ArrayList<Room> getAllRooms() throws DAOException {
        ArrayList<Room> rooms = new ArrayList<>();
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery(SELECT_ALL_ROOMS);
            while (resultSet.next()) {
                int id = resultSet.getInt(ID);
                int number = resultSet.getInt(NUMBER);
                String type = resultSet.getString(TYPE);
                rooms.add(new Room(id, number, type));
            }
        } catch (SQLException e) {
            throw new DAOException("An exception while getting all rooms in the hotel " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return rooms;
    }

    /**
     * Deleting the room from the database by its ID
     * @param id room's id
     */
    @Override
    public boolean deleteRoom(int id) {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ROOM)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.error("An exception while deleting the room " + e);
        }
        return false;
    }

    /**
     * Update the room's type
     * @param id room's id
     * @param type new room's type
     */
    @Override
    public void updateRoom(int id, String type) throws DAOException {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ROOM)) {
            preparedStatement.setString(1, type);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("An exception while updating the room " + id + " with the exception " + e);
        }
    }

    /**
     * Getting the part of rooms starts with {@param offset} to {@param offset} + {@param noOfRecords}
     * @param offset the number of the record with which we started
     * @param noOfRecords number of the records we are getting
     * @return rooms
     * @throws DAOException
     */
    @Override
    public ArrayList<Room> getAllRooms(int offset, int noOfRecords) throws DAOException {
        ArrayList<Room> rooms = new ArrayList<>();
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ROOMS_WITH_OFFSET)) {
            preparedStatement.setInt(1, offset);
            preparedStatement.setInt(2, noOfRecords);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt(ID);
                String type = resultSet.getString(TYPE);
                int number = resultSet.getInt(NUMBER);
                rooms.add(new Room(id, number, type));
            }
        } catch (SQLException e) {
            throw new DAOException("Can not to get all rooms with offset " + offset);
        } finally {
            closeResultSet(resultSet);
        }
        return rooms;
    }

    /**
     * Getting the number of the rooms in the database
     * @return number of the rooms
     * @throws DAOException
     */
    @Override
    public int getNumberOfRooms() throws DAOException {
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery("SELECT count(*) FROM room;");
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException("Can not to read the number of the records");
        } finally {
            closeResultSet(resultSet);
        }
        return 0;
    }

    /**
     * Getting all information about room including its
     * capacity and cost
     * @param connection where we are getting information
     * @param query that we want to invoke
     * @param type of the room
     * @return rooms
     */
    private ArrayList<Room> selectRoomsWithFullInfo(Connection connection, PreparedStatement query, String type) throws DAOException {
        ArrayList<Room> rooms = new ArrayList<>();
        ResultSet resultSet = null;
        try(PreparedStatement preparedStatement = connection.prepareStatement(GET_ROOM_TYPE_PARAMS)) {
            preparedStatement.setString(1, type);
            resultSet = preparedStatement.executeQuery();
            int capacity = 0;
            int cost = 0;
            if (resultSet.next()) {
                capacity = resultSet.getInt(CAPACITY);
                cost = resultSet.getInt(COST);
            }
            resultSet = query.executeQuery();
            while (resultSet.next()) {
                rooms.add(new Room(resultSet.getInt(ID), resultSet.getInt(NUMBER), cost, capacity, type));
            }
        } catch (SQLException e) {
            throw new DAOException("An exception while selecting rooms " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return rooms;
    }

    /**
     * Selecting all rooms by given type
     * @param connection where we are getting information
     * @param type given type
     * @return rooms
     */
    private ArrayList<Integer> selectRoomsIDByType(Connection connection, String type) throws DAOException {
        ArrayList<Integer> ids = new ArrayList<>();
        ResultSet resultSet = null;
        try(PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ROOMS_ID_BY_TYPE)) {
            preparedStatement.setString(1, type);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ids.add(resultSet.getInt(ID));
            }
        } catch (SQLException e) {
            throw new DAOException("An exception while selecting all rooms by given type " + type + " " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return ids;
    }

    /**
     * Closing the result set
     * @param resultSet set that we are closing
     */
    private void closeResultSet(ResultSet resultSet) throws DAOException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOException("There was an exception while closing result set : " + e);
            }
        }
    }
}
