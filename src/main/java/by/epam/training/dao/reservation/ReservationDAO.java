package by.epam.training.dao.reservation;

import by.epam.training.dao.exception.DAOException;
import by.epam.training.enums.Status;
import by.epam.training.entities.Reservation;

import java.util.ArrayList;

/**
 * Interface for DAO pattern for reservations
 */
public interface ReservationDAO {

    /**
     * Adding the record to the database
     * @param reservation reservation that we are adding
     * @return true if the reservation added and false otherwise
     */
    boolean addRecord(Reservation reservation) throws DAOException;

    /**
     * Getting the reservation from the database by its ID
     * @param id it of the reservation
     * @return reservation
     */
    Reservation getRecordByID(int id) throws DAOException;

    /**
     * Updating the reservation by its ID in the database
     * @param reservation that is updating
     */
    void updateRecord(Reservation reservation) throws DAOException;

    /**
     * Deleting the reservation by its ID
     * @param id of the reservation
     */
    void deleteRecordByID(int id) throws DAOException;

    /**
     * Deleting all records belonging to the user
     * @param id user's ID
     */
    void deleteRecordsByUserID(int id) throws DAOException;

    /**
     * Changing the reservation status to CONFIRMED
     * @param reservation that we are managing
     */
    void changeRecordStatus(Reservation reservation) throws DAOException;

    /**
     * Getting all reservation belonging to the user
     * @param id user's id
     * @return user's reservations
     */
    ArrayList<Reservation> getAllUserReservations(int id) throws DAOException;

    /**
     * Getting all users' reservations
     * @return ArrayList of users' reservations
     */
    ArrayList<Reservation> getAllReservations() throws DAOException;

    /**
     * Getting all reservations by Status
     * @param status status
     * @return reservations
     */
    ArrayList<Reservation> getAllReservationsByStatus(Status status) throws DAOException;
}
