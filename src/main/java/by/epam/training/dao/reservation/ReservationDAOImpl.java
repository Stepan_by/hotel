package by.epam.training.dao.reservation;

import by.epam.training.dao.exception.DAOException;
import by.epam.training.enums.Status;
import by.epam.training.entities.Reservation;
import by.epam.training.pool.DBConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static by.epam.training.enums.Status.CONFIRMED;

/**
 * Implementation of DAO pattern for reservations
 */
public class ReservationDAOImpl implements ReservationDAO {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-mm-dd");
    private static final Logger LOGGER = Logger.getLogger(ReservationDAOImpl.class);
    
    //Attributes
    public static final String ID = "id";
    public static final String DATE_IN = "date_in";
    public static final String DATE_OUT = "date_out";
    public static final String TYPE = "type";
    public static final String STATUS_NAME = "status_name";
    public static final String PERSONS_NUMBER = "persons_number";
    public static final String USER_ID = "user_id";
    
    /**
     * Query for adding the reservation to database
     */
    private static final String INSERT_RECORD =
            "INSERT INTO reservation VALUES " +
            "(0, ?, ?, (SELECT id FROM room_type WHERE name = ?), ?, (SELECT id FROM reservation_status WHERE status_name = ?), ?);";

    /**
     * Query for selecting the reservation from database by its ID
     */
    private static final String SELECT_RESERVATION_BY_ID =
            "SELECT reservation.*, room_type.name AS type, reservation_status.status_name FROM hotel.reservation " +
            "JOIN room_type ON reservation.room_type = room_type.id " +
            "JOIN reservation_status ON reservation_status.id = reservation.status " +
            "WHERE reservation.id = ?;";

    /**
     * Query for updating the reservation in the database
     */
    private static final String UPDATE_RECORD_BY_ID =
            "UPDATE reservation " +
            "JOIN room_type ON room_type.name = ? " +
            "SET date_in = ?, date_out = ?, persons_number = ?, reservation.room_type = room_type.id " +
            "WHERE reservation.id = ?;";

    /**
     * Query for deleting the reservation from database by its ID
     */
    private static final String DELETE_RECORD_BY_ID =
            "DELETE FROM reservation WHERE id = ?;";

    /**
     * Query for deleting the reservation from database by user's ID
     */
    private static final String DELETE_RECORDS_BY_USER_ID =
            "DELETE FROM reservation WHERE user_id = ?;";

    /**
     * Query for selecting all reservations in the database
     */
    private static final String SELECT_ALL_RESERVATIONS =
            "SELECT reservation.*, room_type.name AS type, reservation_status.status_name " +
            "FROM hotel.reservation " +
            "JOIN room_type ON reservation.room_type = room_type.id " +
            "JOIN reservation_status ON reservation_status.id = reservation.status;";

    /**
     * Query for changing the reservation's status to CONFIRMED
     */
    private static final String CHANGE_RECORD_STATUS =
            "UPDATE reservation " +
            "JOIN reservation_status ON reservation_status.id = (SELECT id FROM reservation_status WHERE status_name = ?) " +
            "SET reservation.status = reservation_status.id " +
            "WHERE reservation.id = ?;";

    /**
     * Query for selecting all reservations by the status
     */
    private static final String SELECT_ALL_RESERVATIONS_BY_STATUS =
            "SELECT reservation.*, room_type.name AS type, reservation_status.status_name " +
            "FROM hotel.reservation " +
            "JOIN room_type ON reservation.room_type = room_type.id " +
            "JOIN reservation_status ON reservation_status.id = reservation.status " +
            "WHERE status_name = ?;";

    /**
     * Query for selecting all user's reservations
     */
    private static final String SELECT_RESERVATIONS_BY_USER_ID =
            "SELECT reservation.*, room_type.name AS type, reservation_status.status_name " +
            "FROM hotel.reservation " +
            "JOIN room_type ON reservation.room_type = room_type.id " +
            "JOIN reservation_status ON reservation_status.id = reservation.status " +
            "WHERE user_id = ?;";

    /**
    /**
     * Adding the record to the database
     * @param reservation reservation that we are adding
     * @return true if the record added
     */
    @Override
    public boolean addRecord(Reservation reservation) {
        try(Connection connection = DBConnectionPool.getConnection(); 
            PreparedStatement statement = connection.prepareStatement(INSERT_RECORD)) {
            statement.setString(1, DATE_FORMAT.format(reservation.getDateFrom()));
            statement.setString(2, DATE_FORMAT.format(reservation.getDateTo()));
            statement.setString(3, String.valueOf(reservation.getType()));
            statement.setString(4, String.valueOf(reservation.getUserId()));
            statement.setString(5, String.valueOf(reservation.getStatus()));
            statement.setString(6, String.valueOf(reservation.getNumberOfPersons()));
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.error("Exception while inserting a reservation to the database : " + e);
        }
        return false;
    }

    /**
     * Getting the reservation from database by its ID
     * @param id it of the reservation
     * @return reservation
     */
    @Override
    public Reservation getRecordByID(int id) throws DAOException {
        Reservation reservation = null;
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_RESERVATION_BY_ID)) {
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                reservation = parseReservationFromResultSet(resultSet);
            }
        } catch (SQLException | ParseException e) {
            throw new DAOException("An exception while getting reservation by id " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return reservation;
    }

    /**
     * Updating the reservation in the database
     * @param reservation that is updating
     */
    @Override
    public void updateRecord(Reservation reservation) throws DAOException {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(UPDATE_RECORD_BY_ID)) {
            statement.setString(1, reservation.getType());
            statement.setString(2, DATE_FORMAT.format(reservation.getDateFrom()));
            statement.setString(3, DATE_FORMAT.format(reservation.getDateTo()));
            statement.setInt(4, reservation.getNumberOfPersons());
            statement.setInt(5, reservation.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception while updating record " + e);
        }
    }

    /**
     * Deleting the reservation in the database by its ID
     * @param id of the reservation
     */
    @Override
    public void deleteRecordByID(int id) throws DAOException {
        deleteRecords(id, DELETE_RECORD_BY_ID);
    }

    /**
     * Deleting the reservation in the database by user's ID
     * @param id user's ID
     */
    @Override
    public void deleteRecordsByUserID(int id) throws DAOException {
        deleteRecords(id, DELETE_RECORDS_BY_USER_ID);
    }

    /**
     * Changing the reservation's status to CONFIRMED
     * @param reservation that we are managing
     */
    @Override
    public void changeRecordStatus(Reservation reservation) throws DAOException {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(CHANGE_RECORD_STATUS)) {
            statement.setString(1, CONFIRMED.name());
            statement.setInt(2, reservation.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Can't to change a record status " + e);
        }
    }

    /**
     * Getting all user's reservations
     * @param id user's id
     * @return user's reservations
     */
    @Override
    public ArrayList<Reservation> getAllUserReservations(int id) throws DAOException {
        ArrayList<Reservation> reservations = new ArrayList<>();
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_RESERVATIONS_BY_USER_ID)) {
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                reservations.add(parseReservationFromResultSet(resultSet));
            }
        } catch (SQLException | ParseException e) {
            throw new DAOException("There was an exception while getting all users' reservations : " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return reservations;
    }

    /**
     * Getting all reservation by chosen status
     * @param status status by what we are select the reservations
     * @return reservations
     */
    @Override
    public ArrayList<Reservation> getAllReservationsByStatus(Status status) throws DAOException {
        ArrayList<Reservation> reservations = new ArrayList<>();
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_RESERVATIONS_BY_STATUS)) {
            preparedStatement.setString(1, status.name());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                reservations.add(parseReservationFromResultSet(resultSet));
            }
        } catch (SQLException | ParseException e) {
            throw new DAOException("There was an exception while getting all reservations : " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return reservations;
    }

    /**
     * Getting all reservations from database
     * @return reservations
     */
    @Override
    public ArrayList<Reservation> getAllReservations() throws DAOException {
        ArrayList<Reservation> reservations = new ArrayList<>();
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery(SELECT_ALL_RESERVATIONS);
            while (resultSet.next()) {
                reservations.add(parseReservationFromResultSet(resultSet));
            }
        } catch (SQLException | ParseException e) {
            throw new DAOException("There was an exception while getting all reservations : " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return reservations;
    }

    /**
     * Closing the result set
     * @param resultSet set that we are closing
     */
    private void closeResultSet(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                LOGGER.error("There was an exception while closing result set : " + e);
            }
        }
    }

    /**
     * Deleting the record by ID
     * @param id record's ID
     * @param query which will be executed
     */
    private void deleteRecords(int id, String query) throws DAOException {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception while deleting reservation " + e);
        }
    }

    /**
     * Method for getting the information from the Result Set
     * and setting it to reservation object
     * @param resultSet container which contains the information for parsing
     * @return reservation
     * @throws SQLException
     * @throws ParseException
     */
    private Reservation parseReservationFromResultSet(ResultSet resultSet) throws SQLException, ParseException {
        int reservationID = resultSet.getInt(ID);
        Date dateFrom = resultSet.getDate(DATE_IN);
        Date dateTo = resultSet.getDate(DATE_OUT);
        String type = resultSet.getString(TYPE);
        Status status = Status.valueOf(resultSet.getString(STATUS_NAME).toUpperCase());
        int numberOfPersons = resultSet.getInt(PERSONS_NUMBER);
        int userID = resultSet.getInt(USER_ID);
        return new Reservation(reservationID, dateFrom, dateTo, userID, type, status, numberOfPersons);
    }
}
