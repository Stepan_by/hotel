package by.epam.training.dao.user;

import by.epam.training.dao.exception.DAOException;
import by.epam.training.dao.reservation.ReservationDAO;
import by.epam.training.dao.reservation.ReservationDAOImpl;
import by.epam.training.enums.Roles;
import by.epam.training.entities.User;
import by.epam.training.pool.DBConnectionPool;
import by.epam.training.hasher.Hasher;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

public class UserDAOImpl implements UserDAO {

    private static final Logger LOGGER = Logger.getLogger(UserDAOImpl.class);

    /**
     * Query for changing the user's password
     */
    private static final String CHANGE_PASSWORD = "UPDATE user SET password = ? WHERE id = ?;";

    /**
     * Query for checking existing the given email in the database
     */
    private static final String EMAIL_CHECKING = "SELECT id FROM user WHERE email = ? LIMIT 1;";

    /**
     * Query for selecting the user form database by its ID
     */
    private static final String SELECT_USER_BY_ID = "SELECT * FROM user WHERE id = ?;";

    /**
     * Query for selecting the user from database by its email
     */
    private static final String SELECT_USER_BY_EMAIL =
            "SELECT user.*, roles.role AS role FROM user " +
            "JOIN user_role on user.id = user_role.id_user " +
            "JOIN roles on user_role.id_role = roles.id " +
            "WHERE email = ?;";

    /**
     * Query for saving the new user in the database
     */
    private static final String INSERT_USER = "INSERT INTO user (name, email, password) VALUES (?, ?, ?);";

    /**
     * Query for changing the user's role
     */
    private static final String CHANGE_ROLE = "UPDATE user_role " +
            "SET user_role.id_role = (SELECT roles.id FROM roles WHERE role = ?) WHERE id_user = ?;";

    /**
     * Query for removing the user from the database
     */
    private static final String DELETE_USER = "DELETE FROM user WHERE id = ?;";

    /**
     * Query for changing the user's name
     */
    private static final String UPDATE_NAME_QUERY = "UPDATE user SET name=? WHERE email = ?;";

    /**
     * Query for changing the user's email
     */
    private static final String UPDATE_EMAIL_QUERY = "UPDATE user SET email = ? WHERE email = ?;";

    /**
     * Query for selecting all users from the database
     */
    private static final String SELECT_ALL_USERS =
            "SELECT user.id, user.name, user.email, roles.role AS role FROM user " +
            "JOIN user_role ON user_role.id_user = user.id " +
            "JOIN roles ON user_role.id_role = roles.id;";

    //Attributes
    public static final String ID = "id";
    private static final String ROLE = "role";
    private static final String PASSWORD = "password";
    private static final String USER_NAME = "name";
    private static final String EMAIL = "email";

    public UserDAOImpl() {
    }

    /**
     * Getting hte user by its email and password
     * @param email user's email
     * @param password user's password
     * @return {@code User} object and NULL if the user with
     * such email don't exists or password is not correct
     */
    @Override
    public User getUserByLoginPassword(String email, String password) throws DAOException {
        ResultSet resultSet = null;
        try (Connection con = DBConnectionPool.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_USER_BY_EMAIL)) {
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            if (resultSet == null) {
                return null;
            } else {
                resultSet.next();
            }
            if (!Hasher.isPasswordsEquals(password, resultSet.getString(PASSWORD))) {
                return null;
            }
            return new User(Integer.parseInt(resultSet.getString(ID)),
                    resultSet.getString(USER_NAME), email, password,
                    Roles.valueOf(resultSet.getString(ROLE).toUpperCase()));
        } catch (SQLException e) {
            throw new DAOException("An exception in finding user : " + e);
        } finally {
            closeResultSet(resultSet);
        }
    }

    /**
     * Getting the user by its ID
     * @param id user's id
     * @return {@code User} object
     */
    @Override
    public User getUserByID(int id) throws DAOException {
        User user = new User();
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_USER_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt(ID));
                user.setEmail(resultSet.getString(EMAIL));
                user.setName(resultSet.getString(USER_NAME));
            }
        } catch (SQLException e) {
            throw new DAOException("An exception while getting user by id : " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return user;
    }

    /**
     * Updating the user's password
     * @param user user which we are managing
     * @param password new password
     * @throws DAOException
     */
    @Override
    public void updatePassword(User user, String password) throws DAOException {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(CHANGE_PASSWORD)) {
            statement.setString(1, Hasher.hashPassword(password));
            statement.setInt(2, user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("An exception while changing password " + e);
        }
    }

    /**
     * Changing the user's role
     * @param userID user's ID
     * @param role new user's role
     */
    @Override
    public void changeRoleByUserID(int userID, Roles role) throws DAOException {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(CHANGE_ROLE)) {
            preparedStatement.setString(1, role.name());
            preparedStatement.setInt(2, userID);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("An exception while changing the user's " + userID + " role");
        }
    }

    /**
     * Saving the user in the database
     * @param user new user
     * @return true if the user saved
     */
    @Override
    public boolean save(User user) {
        if (user == null) {
            return false;
        }
        if (!isEmailExists(user.getEmail())) {
            ResultSet resultSet = null;
            try (Connection connection = DBConnectionPool.getConnection();
                 PreparedStatement statement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, user.getName());
                statement.setString(2, user.getEmail());
                statement.setString(3, Hasher.hashPassword(user.getPassword()));
                statement.executeUpdate();
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    user.setId(resultSet.getInt(1));
                }
                return true;
            } catch (SQLException e) {
                LOGGER.error("An exception while saving the user " + user.getEmail() + " : " +  e);
            } finally {
                closeResultSet(resultSet);
            }
        }
        return false;
    }

    /**
     * Checking if the email exists in the database
     * @param email email
     * @return true if the email is not exist
     */
    private boolean isEmailExists(String email) {
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(EMAIL_CHECKING)) {
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            LOGGER.error("An exception while checking user existing : " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return false;
    }

    /**
     * Removing the user from the database by its ID
     * @param userId user's ID
     */
    @Override
    public void delete(int userId) throws DAOException {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement userStatement = connection.prepareStatement(DELETE_USER)) {
            ReservationDAO dao = new ReservationDAOImpl();
            dao.deleteRecordsByUserID(userId);
            userStatement.setInt(1, userId);
            userStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("There was an exception while deleting " + userId + " : " + e);
        }
    }

    /**
     * Changing the user's name
     * @param userEmail user's email
     * @param name new user's name
     */
    @Override
    public void updateName(String userEmail, String name) throws DAOException {
        try(Connection connection = DBConnectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(UPDATE_NAME_QUERY)) {
            statement.setString(1, name);
            statement.setString(2, userEmail);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("An exception in updating user name : " + e);
        }
    }

    /**
     * Changing the user's email
     * @param lastEmail last user's email
     * @param curEmail new user's email
     * @return true if the email changed
     */
    @Override
    public boolean updateEmail(String lastEmail, String curEmail) throws DAOException {
        if (!isEmailExists(curEmail)) {
            try (Connection connection = DBConnectionPool.getConnection();
                 PreparedStatement statement = connection.prepareStatement(UPDATE_EMAIL_QUERY)) {
                statement.setString(1, curEmail);
                statement.setString(2, lastEmail);
                statement.executeUpdate();
                return true;
            } catch (SQLException e) {
                throw new DAOException("An exception in updating user email : " + e);
            }
        }
        return false;
    }

    /**
     * Getting all users from the database
     * @return users
     * @throws DAOException
     */
    @Override
    public ArrayList<User> getAllUsers() throws DAOException {
        ArrayList<User> users = new ArrayList<>();
        ResultSet resultSet = null;
        try(Connection connection = DBConnectionPool.getConnection();
            Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery(SELECT_ALL_USERS);
            while (resultSet.next()) {
                users.add(new User(resultSet.getInt(ID),
                        resultSet.getString(USER_NAME),
                        resultSet.getString(EMAIL),
                        Roles.valueOf(resultSet.getString(ROLE).toUpperCase())));
            }
        } catch (SQLException e) {
            throw new DAOException("An exception while getting all users " + e);
        } finally {
            closeResultSet(resultSet);
        }
        return users;
    }

    /**
     * Closing the result ser
     * @param resultSet set that we are closing
     */
    private void closeResultSet(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                LOGGER.error("There was an exception while closing result set : " + e);
            }
        }
    }
}
