package by.epam.training.dao.user;

import by.epam.training.dao.exception.DAOException;
import by.epam.training.enums.Roles;
import by.epam.training.entities.User;

import java.util.ArrayList;

/**
 * Interface for DAO pattern for users
 */
public interface UserDAO {

    /**
     * Getting the user from database by his email and password
     * @param email user's email
     * @param password user's password
     * @return {@code User} object or null if the user wasn't find
     */
    User getUserByLoginPassword(String email, String password) throws DAOException;

    /**
     * Getting the user by his ID
     * @param id user's id
     * @return user
     */
    User getUserByID(int id) throws DAOException;

    /**
     * Updating the user's password
     * @param user user which we are managing
     * @param password new password
     */
    void updatePassword(User user, String password) throws DAOException;

    /**
     * Saving the new user in the database
     * @param user new user
     * @return true if the user saved
     */
    boolean save(User user);

    /**
     * Deleting the user form database by its ID
     * @param userId user's ID
     */
    void delete(int userId) throws DAOException;

    /**
     * Changing the user's name
     * @param userEmail user's email
     * @param name new user's name
     */
    void updateName(String userEmail, String name) throws DAOException;

    /**
     * Changing the user's email
     * @param lastEmail last user's email
     * @param curEmail new user's email
     * @return true if the email changed
     */
    boolean updateEmail(String lastEmail, String curEmail) throws DAOException;

    /**
     * Getting all users from database
     * @return users
     */
    ArrayList<User> getAllUsers() throws DAOException;

    /**
     * Changing the user's role by its ID
     * @param userID user's ID
     * @param role new user's role
     */
    void changeRoleByUserID(int userID, Roles role) throws DAOException;
}
