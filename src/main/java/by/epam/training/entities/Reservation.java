package by.epam.training.entities;

import by.epam.training.enums.Status;

import java.util.Date;

public class Reservation {

    private int id;
    private Date dateFrom;
    private Date dateTo;
    private int userId;
    private String type;
    private Status status;
    private int numberOfPersons;

    public Reservation() {
    }

    public Reservation(int id, Date dateFrom, Date dateTo, int userId, String type, Status status, int numberOfPersons) {
        this.id = id;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.userId = userId;
        this.type = type;
        this.status = status;
        this.numberOfPersons = numberOfPersons;
    }

    public Reservation(Date dateFrom, Date dateTo, int userId, String type, Status status, int numberOfPersons) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.userId = userId;
        this.type = type;
        this.status = status;
        this.numberOfPersons = numberOfPersons;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", userId=" + userId +
                ", type=" + type +
                ", status=" + status +
                ", numberOfPersons=" + numberOfPersons +
                '}';
    }
}
