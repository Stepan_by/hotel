package by.epam.training.entities;

public class Room {

    private int id;
    private int number;
    private int cost;
    private int capacity;
    private String type;

    public Room() {
    }

    public Room(int id, int number, String type) {
        this.id = id;
        this.number = number;
        this.type = type;
    }

    public Room(int id, int number, int cost, int capacity, String type) {
        this.id = id;
        this.number = number;
        this.cost = cost;
        this.capacity = capacity;
        this.type = type;
    }

    public Room(int id, int number) {
        this.id = id;
        this.number = number;
    }

    public Room(int id, int number, int cost, int capacity) {
        this.id = id;
        this.number = number;
        this.cost = cost;
        this.capacity = capacity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", number=" + number +
                ", cost=" + cost +
                ", capacity=" + capacity +
                ", type='" + type + '\'' +
                '}';
    }
}
