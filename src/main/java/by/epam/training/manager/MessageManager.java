package by.epam.training.manager;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Manager for getting the messages in the current application's language
 */
public class MessageManager {

    /**
     * Messages property's name
     */
    public static final String MESSAGES = "messages";

    private static ResourceBundle bundle = ResourceBundle.getBundle(MESSAGES);

    /**
     * Change the bundle for new language
     * @param locale new locale
     */
    public static void changeLocale(Locale locale) {
        bundle = ResourceBundle.getBundle(MESSAGES, locale);
    }

    /**
     * Getting the message
     * @param message message's path in the property
     * @return message
     */
    public static String getMessage(String message) {
        return bundle.getString(message);
    }
}
