package by.epam.training.manager;

import java.util.ResourceBundle;

/**
 * Manager for getting the application's pages
 */
public class PagesManager {

    /**
     * Property which contains pages
     */
    private static final ResourceBundle PAGES_BUNDLE = ResourceBundle.getBundle("pages");

    public static String getPage(String page) {
        return PAGES_BUNDLE.getString(page);
    }
}
