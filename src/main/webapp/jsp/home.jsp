<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="content">
<html>
    <head>
        <title><fmt:message key="menu.home"/></title>
        <meta charset="UTF-8"/>
        <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
        <script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
        <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
        <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
    </head>
    <body>

        <%--Header--%>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <main>

            <%--Menu--%>
            <%@include file="../WEB-INF/jspf/menu.jspf" %>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                <%--Carousel--%>
                <div class="carousel-inner">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    </ol>
                    <div class="item active">
                        <img src="<c:url value="/image/carousel/carousel_1.jpg"/>">
                        <div class="carousel-caption">
                            <h2><fmt:message key="image.kitchenPartOne"/></h2>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<c:url value="/image/carousel/carousel_2.jpg"/>">
                        <div class="carousel-caption">
                            <h2><fmt:message key="image.kitchenPartTwo"/></h2>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<c:url value="/image/carousel/carousel_3.jpg"/>">
                        <div class="carousel-caption">
                            <h2><fmt:message key="image.livingRoomPartOne"/></h2>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<c:url value="/image/carousel/carousel_4.jpg"/>">
                        <div class="carousel-caption">
                            <h2><fmt:message key="image.livingRoomPartTwo"/></h2>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <%--Footer--%>
        <%@include file="../WEB-INF/jspf/footer.jspf" %>
    </body>
</html>
</fmt:bundle>
