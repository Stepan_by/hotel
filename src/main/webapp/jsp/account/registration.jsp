<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="content">
<html>
    <head>
        <title><fmt:message key="header.registration"/></title>
        <meta charset="UTF-8">
        <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
        <script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
        <script src="<c:url value="/js/checkThePasswords.js"/>"></script>
        <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
    </head>
    <body>

        <%--Header--%>
        <%@include file="/WEB-INF/jspf/header.jspf"%>

        <main>

            <%--Menu--%>
            <%@include file="/WEB-INF/jspf/menu.jspf"%>

             <div class="jumbotron">
                <%--Error message--%>
                <div class="error-message">
                    ${sessionScope.message}
                </div>

                <form class="form-horizontal" action="<c:url value="/controller"/>" method="post" onsubmit="return validate();">
                    <fieldset>

                        <!-- Form Name -->
                        <legend><fmt:message key="user.reg.title"/></legend>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">
                                <fmt:message key="user.reg.name"/>
                            </label>
                            <div class="col-md-6">
                                <input id="name"
                                       name="name"
                                       type="text"
                                       placeholder="<fmt:message key="user.reg.placeholder.name"/>"
                                       class="form-control input-md" required>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="email">
                                <fmt:message key="user.reg.email"/>
                            </label>
                            <div class="col-md-6">
                                <input id="email"
                                       name="email"
                                       type="email"
                                       placeholder="<fmt:message key="user.reg.placeholder.email"/>"
                                       class="form-control input-md" required>
                            </div>
                        </div>

                        <!-- Password input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="password">
                                <fmt:message key="user.reg.password"/>
                            </label>
                            <div class="col-md-6">
                                <input id="password"
                                       name="password"
                                       type="password"
                                       placeholder="<fmt:message key="user.reg.password"/>"
                                       class="form-control input-md" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="password">
                                <fmt:message key="user.reg.confirm"/>
                            </label>
                            <div class="col-md-6">
                                <input id="confirm_password"
                                       type="password"
                                       placeholder="<fmt:message key="user.reg.confirm"/>"
                                       class="form-control input-md" required>
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-6">
                                <button id="submit_btn"
                                        type="submit"
                                        name="command"
                                        value="registration"
                                        class="btn btn-danger btn-lg">
                                    <fmt:message key="user.reg.title"/>
                                </button>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
        </main>
        <%--Footer--%>
        <%@include file="/WEB-INF/jspf/footer.jspf"%>
    </body>
</html>
</fmt:bundle>