<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:bundle basename="content">
    <html>
        <head>
            <meta charset="UTF-8">
            <title><fmt:message key="header.login"/></title>
            <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
            <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
            <script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
            <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
            <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
        </head>
        <body>
            <%--Header--%>
            <%@include file="/WEB-INF/jspf/header.jspf"%>
            <main>
                <%--Menu--%>
                <%@include file="/WEB-INF/jspf/menu.jspf"%>
                <div class="jumbotron">
                    <%--Message--%>
                    <div class="error-message">
                        ${sessionScope.message}
                    </div>

                    <%--Login form--%>
                    <form class="form-horizontal" method="post" action="<c:url value="/controller"/>">
                        <fieldset>

                            <!-- Form Name -->
                            <legend><fmt:message key="user.login.title"/></legend>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="email">
                                    <fmt:message key="user.login.email"/>
                                </label>
                                <div class="col-md-6">
                                    <input id="email"
                                           name="email"
                                           type="email"
                                           placeholder="<fmt:message key="user.reg.placeholder.email"/>"
                                           value="${cookie.email.value}"
                                           class="form-control input-md" required>
                                </div>
                            </div>
                            <!-- Password input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password">
                                    <fmt:message key="user.login.password"/>
                                </label>
                                <div class="col-md-6">
                                    <input id="password"
                                           name="password"
                                           type="password"
                                           placeholder="<fmt:message key="user.reg.password"/>"
                                           value="${cookie.password.value}"
                                           class="form-control input-md" required>
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-4">
                                    <button id="signup"
                                            name="command"
                                            value="login"
                                            class="btn btn-danger btn-lg">
                                        <fmt:message key="user.login.submit"/>
                                    </button>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </main>
            <%--Footer--%>
            <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </body>
    </html>
</fmt:bundle>