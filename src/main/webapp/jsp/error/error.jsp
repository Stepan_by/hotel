<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="messages">
<html>
    <head>
        <meta charset="UTF-8"/>
        <title><fmt:message key="error.title"/></title>
        <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
        <script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
        <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
        <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
    </head>
    <body>
        <%--Header--%>
        <%@include file="/WEB-INF/jspf/header.jspf"%>
        <main>
            <%--Menu--%>
            <%@include file="/WEB-INF/jspf/menu.jspf"%>
            <div class="jumbotron">
                <div class="error">
                    <fmt:message key="error.message"/>
                </div>
            </div>
        </main>
        <%--Footer--%>
        <%@include file="/WEB-INF/jspf/footer.jspf"%>
    </body>
</html>
</fmt:bundle>