<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctd" uri="customtags" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="content">
    <html>
        <head>
            <meta charset="UTF-8"/>
            <title><fmt:message key="admin.bookings.title"/></title>
            <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
            <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
            <script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
            <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
            <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
        </head>
        <body>
            <%--Header--%>
            <%@include file="/WEB-INF/jspf/header.jspf"%>

            <main class="wide-view">
                <%--Menu--%>
                <%@include file="/WEB-INF/jspf/menu.jspf"%>
                <div class="jumbotron col-md-12">
                    <%--Title--%>
                    <legend><fmt:message key="user.bookings.title"/></legend>

                    <div class="full-block col-md-12">

                        <%--Bookings in process--%>
                        <div class="booking-block col-md-6">
                            <div class="title">
                                <fmt:message key="booking.inProcess"/>
                            </div>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>№</th>
                                        <th><fmt:message key="booking.type"/></th>
                                        <th><fmt:message key="booking.persons"/></th>
                                        <th><fmt:message key="admin.bookings.controlPanel"/> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="reservation" items="${requestScope.inProcess}">
                                        <tr>
                                            <ctd:resInfo reservation="${reservation}" showDate="false"/>
                                            <td style="display: inline-flex">
                                                <form action="<c:url value="/controller"/>">
                                                    <input type="hidden" name="id" value="${reservation.id}"/>
                                                    <button id="correct" name="command"
                                                            value="correct_booking"
                                                            class="btn btn-primary">
                                                        <fmt:message key="user.bookings.correct"/>
                                                    </button>
                                                    <button name="command" value="delete_booking" class="btn btn-danger">
                                                        <fmt:message key="admin.bookings.delete"/>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>

                        <%--Bookings confirmed--%>
                        <div class="booking-block col-md-6">
                            <div class="title">
                                <fmt:message key="booking.confirmed"/>
                            </div>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>№</th>
                                        <th><fmt:message key="booking.type"/></th>
                                        <th><fmt:message key="booking.persons"/></th>
                                        <th><fmt:message key="admin.bookings.controlPanel"/> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="reservation" items="${requestScope.confirmed}">
                                        <tr>
                                            <ctd:resInfo reservation="${reservation}" showDate="false"/>
                                            <td style="display: inline-flex">
                                                <form action="<c:url value="/controller"/>">
                                                    <input type="hidden" name="id" value="${reservation.id}"/>
                                                    <button id="pay" name="command" value="pay_info" class="btn btn-primary">
                                                        <fmt:message key="user.bookings.pay"/>
                                                    </button>
                                                    <button name="command" value="delete_booking" class="btn btn-danger">
                                                        <fmt:message key="admin.bookings.delete"/>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </main>
            <%--Footer--%>
            <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </body>
    </html>
</fmt:bundle>