<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="content">
<html>
    <head>
        <meta charset="UTF-8"/>
        <title><fmt:message key="user.payment.title"/></title>
        <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
        <script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
        <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
        <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
    </head>
    <body>
        <%--Header--%>
        <%@include file="/WEB-INF/jspf/header.jspf"%>
        <main>
            <%--Menu--%>
            <%@include file="/WEB-INF/jspf/menu.jspf"%>
            <div class="jumbotron col-md-12">
                <%--Title--%>
                <legend><fmt:message key="user.payment.title"/></legend>

                <%--Order's info--%>
                <div class="col-md-6">
                    <%--Title--%>
                    <div class="title">
                        <fmt:message key="admin.bookingInfo.bookingInfoTitle"/>
                    </div>
                    <%--Booking's ID--%>
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-label">№</label>
                        <span class="col-md-8">${requestScope.reservation.id}</span>
                    </div>
                    <%--Date since--%>
                     <div class="form-group col-md-12">
                        <label class="col-md-4 control-label"><fmt:message key="booking.since"/></label>
                        <span class="col-md-8">${requestScope.reservation.getDateFrom()}</span>
                    </div>
                    <%--Date till--%>
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-label"><fmt:message key="booking.till"/></label>
                        <span class="col-md-8">${requestScope.reservation.getDateTo()}</span>
                    </div>
                    <%--Room type--%>
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-label"><fmt:message key="booking.type"/></label>
                        <span class="col-md-8">${requestScope.reservation.type}</span>
                    </div>
                    <%--Number of the persons--%>
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-label"><fmt:message key="booking.persons"/></label>
                        <span class="col-md-8">${requestScope.reservation.numberOfPersons}</span>
                    </div>
                </div>

                <%--Rooms--%>
                <div class="col-md-6">
                    <%--Ttile--%>
                    <div class="title">
                        <fmt:message key="user.payment.roomsInfo"/>
                    </div>
                    <%--The list of the given rooms--%>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>№</th>
                                <th><fmt:message key="room.type"/></th>
                                <th><fmt:message key="room.cost"/></th>
                                <th><fmt:message key="room.capacity"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="room" items="${requestScope.rooms}">
                                <tr>
                                    <td>${room.number}</td>
                                    <td>${room.type}</td>
                                    <td>${room.cost}</td>
                                    <td>${room.capacity}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <%--Total cost--%>
                    <div class="title">
                       <fmt:message key="admin.bookingInfo.cost"/> ${requestScope.cost}
                    </div>
                </div>

                <%--User's payment info--%>
                <div class="full-block col-md-12">
                    <form method="post" action="<c:url value="/controller"/>">
                        <fieldset>
                            <%--Title--%>
                            <legend><fmt:message key="user.payment.choosePayment"/></legend>
                            <%--Card--%>
                            <div class="cc-selector">
                                <input checked id="visa" type="radio" name="credit-card" value="visa" />
                                <label class="drinkcard-cc visa" for="visa"></label>
                                <input id="mastercard" type="radio" name="credit-card" value="mastercard" />
                                <label class="drinkcard-cc mastercard" for="mastercard"></label>
                            </div>
                            <%--Card's number--%>
                            <div class="form-group col-md-12">
                                <label class="col-md-offset-3 col-md-3" for="number"><fmt:message key="user.payment.card.number"/> :</label>
                                <div class="col-md-5">
                                    <input id="number" name="number"
                                           maxlength="19" pattern="\d{4}-\d{4}-\d{4}-\d{4}" placeholder="#### - #### - #### - ####"
                                           type="text" class="input-md form-control" required/>
                                </div>
                            </div>
                            <%--Owner--%>
                            <div class="form-group col-md-12">
                                <label class="col-md-offset-3 col-md-3" for="owner"><fmt:message key="user.payment.card.owner"/> :</label>
                                <div class="col-md-5">
                                    <input id="owner" name="owner" type="text" class="input-md form-control" required/>
                                </div>
                            </div>
                            <%--Confirm button--%>
                            <div class="col-md-offset-4 col-md-12">
                                <input type="hidden" name="id" value="${requestScope.reservation.id}"/>
                                <button id="pay" name="command" value="pay_booking" class="btn btn-primary col-md-4" style="margin-top: 2%">
                                    <fmt:message key="user.bookings.pay"/>
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <%--Footer--%>
            <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </main>
    </body>
</html>
</fmt:bundle>