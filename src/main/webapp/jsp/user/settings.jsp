<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="content">
<html>
	<head>
		<meta charset="UTF-8"/>
		<title><fmt:message key="user.settings.title"/></title>
		<link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
		<link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
		<script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
		<script src="<c:url value="/js/bootstrap.min.js"/>"></script>
		<link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
	</head>
	<body>
        <%--Header--%>
		<%@include file="/WEB-INF/jspf/header.jspf"%>
		<main>
            <%--Menu--%>
			<%@include file="/WEB-INF/jspf/menu.jspf"%>
			<div class="jumbotron">

				<%--Error message--%>
				<div class="error-message">
						${sessionScope.message}
				</div>

				<form class="form-horizontal" action="<c:url value="/controller"/>" method="post" >
					<fieldset>

                        <%--Title--%>
						<legend><fmt:message key="user.settings.title"/></legend>

                        <%--Changing name--%>
						<div class="form-group">
							<label class="col-md-offset-2 col-md-4 control-label" for="user-name">
								<fmt:message key="user.settings.changeName"/> :
							</label>
							<div class="col-md-6">
								<input id="user-name"
									   name="user-name"
									   placeholder="<fmt:message key="user.settings.namePlaceholder"/> : ${sessionScope.user.name}"
									   type="text" class="form-control input-md"/>
							</div>
						</div>

                        <%--Changing email--%>
						<div class="form-group">
							<label class="col-md-offset-2 col-md-4 control-label" for="user-name">
								<fmt:message key="user.settings.changeEmail"/> :
							</label>
							<div class="col-md-6">
								<input id="user-email"
									   name="user-email"
									   placeholder="<fmt:message key="user.settings.emailPlaceholder"/> : ${sessionScope.user.email}"
									   type="email"
									   class="form-control input-md"/>
							</div>
						</div>

                        <%--Submit button--%>
						<div class="form-group">
							<div class="col-md-offset-3 col-md-6">
								<button id="confirm"
										name="command"
										value="change_settings"
										type="submit"
										class="btn btn-danger btn-lg">
									<fmt:message key="user.settings.changeSettingsButton"/>
								</button>
							</div>
						</div>

					</fieldset>
				</form>

                <%--Password changing block--%>
				<form class="form-horizontal" method="post" action="<c:url value="/controller"/>">

                    <%--Last password areas--%>
					<div class="form-group">
						<label class="col-md-offset-2 col-md-4 control-label" for="user-name">
							<fmt:message key="user.settings.lastPassword"/> :
						</label>
						<div class="col-md-6">
							<input id="user-old-password"
								   name="user-old-password"
								   placeholder="<fmt:message key="user.settings.lastPassword"/>"
								   type="password"
								   class="form-control input-md"/>
						</div>
					</div>

                    <%--New password--%>
					<div class="form-group">
						<label class="col-md-offset-2 col-md-4 control-label" for="user-name">
							<fmt:message key="user.settings.newPassword"/> :
						</label>
						<div class="col-md-6">
							<input id="user-password"
								   name="user-password"
								   placeholder="<fmt:message key="user.settings.newPassword"/>"
								   type="password"
								   class="form-control input-md"/>
						</div>
					</div>

                    <%--Submit button--%>
					<div class="form-group">
						<div class="col-md-offset-3 col-md-6">
							<button name="command"
									value="change_password"
									class="btn btn-danger btn-lg">
								<fmt:message key="user.settings.changePasswordButton"/>
							</button>
						</div>
					</div>
				</form>

                <%--Account deleting button--%>
				<form class="form-horizontal" method="post" action="<c:url value="/controller"/>">
					<div class="form-group">
						<label class="col-md-offset-2 col-md-4 control-label">
							<fmt:message key="user.settings.deleteMessage"/>
						</label>
						<div class="col-md-5">
							<button id="delete" name="command"
									value="delete_account"
									class="btn btn-danger btn-lg">
								<fmt:message key="user.settings.deleteButton"/>
							</button>
						</div>
					</div>
				</form>
			</div>
		</main>

		<%--Footer--%>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</body>
</html>
</fmt:bundle>