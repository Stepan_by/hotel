<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="content">
<html>
	<head>
		<meta charset="UTF-8"/>
		<title><fmt:message key="user.book.title"/></title>
		<link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
		<link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
		<script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
		<script src="<c:url value="/js/checkTheDate.js"/>"></script>
		<link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
	</head>
	<body>
		<%--Header--%>
		<%@include file="/WEB-INF/jspf/header.jspf"%>
		<main>

			<%--Menu--%>
			<%@include file="/WEB-INF/jspf/menu.jspf"%>

			<div>
				<div class="jumbotron">
					<form class="form-horizontal" method="post" action="<c:url value="/controller"/>"
						  onsubmit="return checkTheDate();">
						<fieldset>
							<%--Title--%>
							<legend><fmt:message key="user.book.title"/></legend>

							<%--Number of the persons--%>
							<div class="form-group col-md-12">
								<div class="persons" style="display: flex">
									<label class="col-md-offset-3 col-md-4 control-label"><fmt:message key="booking.persons"/> : </label>
									<select title="selection" name="persons" class="form-control">
										<c:forEach var="i" begin="1" end="10">
											<option>${i}</option>
										</c:forEach>
										<c:if test="${not empty requestScope.selected}">
											<option selected="selected">
												${requestScope.selected}
											</option>
										</c:if>
									</select>
								</div>
							</div>

							<%--Room type--%>
							<div class="form-group col-md-12">
								<label class="col-md-offset-2 col-md-2 control-label"><fmt:message key="booking.type"/> : </label>
								<div class="col-md-6 apartment">
									<label class="radio-inline active">
										<input type="radio" name="type" id="single" value="single" checked/>
										<fmt:message key="room.type.single"/>
									</label>
									<label class="radio-inline">
										<input type="radio" name="type" id="double" value="double"
											   <c:if test="${requestScope.type eq 'double'}">checked</c:if>/>
										<fmt:message key="room.type.double"/>
									</label>
									<label class="radio-inline">
										<input type="radio" name="type" id="family" value="family"
											   <c:if test="${requestScope.type eq 'family'}">checked</c:if>/>
										<fmt:message key="room.type.family"/>
									</label>
								</div>
							</div>

							<%--Day in--%>
							<div class="form-group col-md-12">
								<label class="col-md-offset-2 col-md-4 control-label" for="day-in">
									<fmt:message key="booking.dayIn"/>
								</label>
								<div class="col-md-3">
									<input type="date" value="${requestScope.dayIn}" id="day-in" name="dayIn" required><br><br>
								</div>
							</div>

							<%--Day out--%>
							<div class="form-group col-md-12">
								<label class="col-md-offset-2 col-md-4 control-label" for="day-out">
									<fmt:message key="booking.dayOut"/>
								</label>
								<div class="col-md-3">
									<input type="date" id="day-out" value="${requestScope.dayOut}" name="dayOut" required><br><br>
								</div>
							</div>

							<%--Confirm button--%>
							<c:choose>
								<c:when test="${requestScope.size() eq 0}">
									<div class="form-group col-md-12">
										<div class="col-md-offset-4 col-md-4">
											<button id="send" name="command" value="book" class="btn btn-danger btn-lg">
												<fmt:message key="user.book.send"/>
											</button>
										</div>
									</div>
								</c:when>
								<c:otherwise>
									<div class="form-group col-md-12">
										<div class="col-md-offset-4 col-md-4">
											<input type="hidden" name="reservationID" value="${requestScope.reservationID}"/>
											<button id="change" name="command" value="update_booking" class="btn btn-danger btn-lg">
												<fmt:message key="user.book.change"/>
											</button>
										</div>
									</div>
								</c:otherwise>
							</c:choose>
						</fieldset>
					</form>
				</div>
			</div>
		</main>

		<%--Footer--%>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</body>
</html>
</fmt:bundle>