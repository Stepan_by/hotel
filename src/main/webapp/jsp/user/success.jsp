<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="messages">
<html>
    <head>
        <title><fmt:message key="success.title"/></title>
        <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
        <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
    </head>
    <body>
        <%--Header--%>
        <%@include file="/WEB-INF/jspf/header.jspf"%>
        <main>
            <%--Menu--%>
            <%@include file="/WEB-INF/jspf/menu.jspf"%>
            <div class="jumbotron">
                <div class="success">
                    ${sessionScope.message}
                </div>
            </div>
        </main>
        <%--Footer--%>
        <%@include file="/WEB-INF/jspf/footer.jspf"%>
    </body>
</html>
</fmt:bundle>
