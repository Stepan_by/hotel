<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctd" uri="customtags" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="content">
<html>
    <head>
        <meta charset="UTF-8">
        <title><fmt:message key="admin.roles.title"/></title>
        <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
        <script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
        <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
        <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
    </head>
    <body>
        <%--Header--%>
        <%@include file="/WEB-INF/jspf/header.jspf"%>
        <main>
            <%--Menu--%>
            <%@include file="/WEB-INF/jspf/menu.jspf"%>
            <div class="jumbotron row">
                <%--Title--%>
                <legend><fmt:message key="admin.roles.title"/></legend>

                <div class="full-block col-md-12">
                    <table class="table table-hover">
                       <thead>
                            <tr>
                                <th>№</th>
                                <th><fmt:message key="admin.roles.name"/></th>
                                <th><fmt:message key="admin.roles.email"/></th>
                                <th><fmt:message key="admin.roles.role"/></th>
                            </tr>
                       </thead>
                        <tbody>
                            <c:forEach var="user" items="${sessionScope.users}">
                                <tr>
                                    <td>${user.id}</td>
                                    <td>${user.name}</td>
                                    <td>${user.email}</td>
                                    <td>
                                        <form action="<c:url value="/controller"/>" method="post" id="${user.id}">
                                            <input type="hidden" name="command" value="update_admin_role">
                                            <input type="hidden" name="user" value="${user.id}">
                                            <input type="hidden" name="role" id="role${user.id}">
                                            <div class="dropdown">
                                                <button class="btn btn-default dropdown-toggle"
                                                        type="button"
                                                        data-toggle="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="true">
                                                    ${user.role}
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="javascript:{}" onclick="
                                                                document.getElementById('role${user.id}').setAttribute('value', 'user');
                                                                document.getElementById('${user.id}').submit();">
                                                            <fmt:message key="admin.roles.role.user"/>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:{}" onclick="
                                                                document.getElementById('role${user.id}').setAttribute('value', 'admin');
                                                                document.getElementById('${user.id}').submit();">
                                                            <fmt:message key="admin.roles.role.admin"/>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <%--Footer--%>
            <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </main>
    </body>
</html>
</fmt:bundle>