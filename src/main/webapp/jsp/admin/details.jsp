<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctd" uri="customtags" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="content">
<html>
    <head>
        <meta charset="UTF-8">
        <title><fmt:message key="admin.bookings.details"/></title>
        <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
        <script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
        <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
        <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
    </head>
    <body>

        <%--Header--%>
        <%@include file="/WEB-INF/jspf/header.jspf"%>
        <main>

            <%--Menu--%>
            <%@include file="/WEB-INF/jspf/menu.jspf"%>

            <div class="jumbotron row">

                <%--Error message--%>
                <div class="error-message">
                    ${sessionScope.message}
                </div>

                <c:set var="reservation" value="${sessionScope.reservation}"/>

                <%--Reservation information--%>
                <div class="col-md-12">
                    <div class="title">
                        <fmt:message key="admin.bookingInfo.bookingInfoTitle"/>
                    </div>
                    <table class="table" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <th>№</th>
                                <th><fmt:message key="booking.type"/></th>
                                <th><fmt:message key="booking.persons"/></th>
                                <th><fmt:message key="booking.since"/></th>
                                <th><fmt:message key="booking.till"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <ctd:resInfo reservation="${reservation}" showDate="true"/>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <c:set var="user" value="${sessionScope.reservationUser}"/>

                <%--User information--%>
                <div class="col-md-12">
                    <div class="title">
                        <fmt:message key="admin.bookingInfo.userInfoTitle"/>
                    </div>
                    <table class="table" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <th>№</th>
                                <th><fmt:message key="user.reg.name"/></th>
                                <th><fmt:message key="user.reg.email"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>${user.id}</th>
                                <th>${user.name}</th>
                                <th>${user.email}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <%--Available and chosen rooms--%>
                <div class="full-block col-md-12">

                    <%--Available rooms--%>
                    <div class="rooms-block col-md-6">
                        <div class="title">
                            <fmt:message key="admin.bookingInfo.availableRooms"/>
                        </div>
                        <div class="form-group">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>№</th>
                                        <th><fmt:message key="room.cost"/></th>
                                        <th><fmt:message key="room.capacity"/></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="room" items="${sessionScope.available}">
                                        <tr>
                                            <td>${room.number}</td>
                                            <td>${room.cost}</td>
                                            <td>${room.capacity}</td>
                                            <td>
                                                <form action="<c:url value="/controller"/>" method="post" id="${room.id}">
                                                    <input type="hidden" name="command" value="update_admin_bookings">
                                                    <input type="hidden" name="action" value="add">
                                                    <input type="hidden" name="room" value="${room.number}">
                                                    <a href="javascript:{}"
                                                       onclick="document.getElementById('${room.id}').submit();">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </a>
                                                </form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <%--Chosen rooms--%>
                    <div class="rooms-block col-md-6">
                        <div class="title">
                            <fmt:message key="admin.bookingInfo.chosenRooms"/>
                        </div>
                        <div class="form-group">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>№</th>
                                        <th><fmt:message key="room.cost"/></th>
                                        <th><fmt:message key="room.capacity"/></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="room" items="${sessionScope.chosen}">
                                        <tr>
                                            <td>${room.number}</td>
                                            <td>${room.cost}</td>
                                            <td>${room.capacity}</td>
                                            <td>
                                                <form action="<c:url value="/controller"/>" method="post" id="${room.id}">
                                                    <input type="hidden" name="command" value="update_admin_bookings">
                                                    <input type="hidden" name="action" value="sub">
                                                    <input type="hidden" name="room" value="${room.number}">
                                                    <a href="javascript:{}"
                                                       onclick="document.getElementById('${room.id}').submit();">
                                                        <span class="glyphicon glyphicon-minus"></span>
                                                    </a>
                                                </form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <%--Currect state--%>
                <div class="title">
                    <fmt:message key="admin.bookingInfo.state"/>
                </div>
                <div class="form-group">
                    <div>
                        <fmt:message key="admin.bookingInfo.cost"/> : ${sessionScope.cost}
                    </div>
                    <div>
                        <fmt:message key="admin.bookingInfo.capacity"/> :
                        ${sessionScope.capacity} / ${reservation.numberOfPersons}
                    </div>
                </div>

                <%--Buttons--%>
                <div class="col-md-12" style="margin-top: 1%">
                    <form action="<c:url value="/controller"/>" method="post">
                        <div class="col-md-offset-2 col-md-12">
                            <button class="btn btn-primary btn-lg col-md-3" name="command" value="admin_confirm"><fmt:message key="admin.bookingInfo.confirm"/></button>
                            <button class="btn btn-danger btn-lg col-md-offset-2 col-md-3" name="command" value="admin_deny"><fmt:message key="admin.bookingInfo.deny"/></button>
                        </div>
                    </form>
                </div>

            </div>
            <%--Footer--%>
            <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </main>
    </body>
</html>
</fmt:bundle>
