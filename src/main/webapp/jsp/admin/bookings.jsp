<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctd" uri="customtags" %>
<c:set var="language" value="${sessionScope.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="content">
    <html>
        <head>
            <meta charset="UTF-8">
            <title><fmt:message key="admin.bookings.title"/> </title>
            <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
            <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
            <script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
            <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
            <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
        </head>
        <body>

            <%--Header--%>
            <%@include file="/WEB-INF/jspf/header.jspf"%>
            <main>
                <%--Menu--%>
                <%@include file="/WEB-INF/jspf/menu.jspf"%>
                <div class="jumbotron row">

                    <%--Title--%>
                    <legend><fmt:message key="admin.bookings.title"/></legend>

                    <%--Bookings in process--%>
                    <div class="booking-block col-md-6">
                        <div class="title">
                            <fmt:message key="booking.inProcess"/>
                        </div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th><fmt:message key="admin.bookings.type"/></th>
                                    <th><fmt:message key="admin.bookings.persons"/></th>
                                    <th><fmt:message key="admin.bookings.controlPanel"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="reservation" items="${sessionScope.inProcess}">
                                    <tr>
                                        <ctd:resInfo reservation="${reservation}"/>
                                        <td style="display: inline-flex">
                                            <form action="<c:url value="/controller"/>">
                                                <div>
                                                    <input type="hidden" name="id" value="${reservation.id}"/>
                                                    <button name="command" value="admin_book_info" class="btn btn-primary"><fmt:message key="admin.bookings.details"/></button>
                                                    <button name="command" value="delete_booking" class="btn btn-danger"><fmt:message key="admin.bookings.delete"/></button>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>

                    <%--Confirmed bookings--%>
                    <div class="booking-block col-md-6">
                        <div class="title">
                            <fmt:message key="booking.confirmed"/>
                        </div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th><fmt:message key="admin.bookings.type"/></th>
                                    <th><fmt:message key="admin.bookings.persons"/></th>
                                    <th><fmt:message key="admin.bookings.controlPanel"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="reservation" items="${sessionScope.confirmed}">
                                    <tr>
                                        <ctd:resInfo reservation="${reservation}"/>
                                        <td style="display: inline-flex">
                                            <form action="<c:url value="/controller"/>">
                                                <div>
                                                    <input type="hidden" name="id" value="${reservation.id}"/>
                                                    <button name="command" value="admin_book_info" class="btn btn-primary"><fmt:message key="admin.bookings.details"/></button>
                                                    <button name="command" value="delete_booking" class="btn btn-danger"><fmt:message key="admin.bookings.delete"/></button>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>

                <%--Footer--%>
                <%@include file="/WEB-INF/jspf/footer.jspf"%>
            </main>
        </body>
    </html>
</fmt:bundle>
