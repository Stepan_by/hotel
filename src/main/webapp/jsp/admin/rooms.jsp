<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctd" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:bundle basename="content">
<html>
    <head>
        <meta charset="UTF-8">
        <title><fmt:message key="admin.rooms.title"/></title>
        <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/css/style.css"/>" rel="stylesheet"/>
        <script src="<c:url value="/js/jquery-1.11.3.min.js"/>"></script>
        <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
        <link rel="icon" type="image/png" href="<c:url value="/image/label/ico/label.ico"/>"/>
    </head>
    <body>
        <%--Header--%>
        <%@include file="/WEB-INF/jspf/header.jspf"%>
        <main class="container">
            <%--Menu--%>
            <%@include file="/WEB-INF/jspf/menu.jspf"%>

            <div class="jumbotron row">

                <%--Message--%>
                <div class="error-message">
                    ${sessionScope.message}
                </div>

                <%--Title--%>
                <legend><fmt:message key="admin.rooms.title"/></legend>

                <form action="<c:url value="/controller"/>" method="post" id="addingRoom">
                    <input type="hidden" name="command" value="admin_add_room">
                    <table class="table" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <th><fmt:message key="admin.rooms.number"/></th>
                                <th><fmt:message key="admin.rooms.type"/></th>
                                <th><fmt:message key="admin.rooms.action"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="number" name="number" min="1" required
                                           title="<fmt:message key="admin.rooms.number"/>">
                                </td>
                                <td>
                                    <div>
                                        <label class="radio-menu-item active">
                                            <input type="radio" name="type" value="single" checked/>
                                            <fmt:message key="room.type.single"/>
                                        </label>
                                    </div>
                                    <div>
                                        <label class="radio-menu-item">
                                            <input type="radio" name="type" value="double"/>
                                            <fmt:message key="room.type.double"/>
                                        </label>
                                    </div>
                                    <div>
                                        <label class="radio-menu-item">
                                            <input type="radio" name="type" value="family"/>
                                            <fmt:message key="room.type.family"/>
                                        </label>
                                    </div>
                                </td>
                                <td style="vertical-align: middle">
                                    <a href="javascript:{}" onclick="document.getElementById('addingRoom').submit();">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><fmt:message key="admin.rooms.number"/></th>
                            <th><fmt:message key="admin.rooms.type"/></th>
                            <th><fmt:message key="admin.rooms.action"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="room" items="${sessionScope.rooms}">
                            <tr>
                                <td>${room.number}</td>
                                <td>
                                    <form action="<c:url value="/controller"/>" method="post" id="${room.id}">
                                        <input type="hidden" name="command" value="update_admin_room">
                                        <input type="hidden" name="room" value="${room.id}">
                                        <input type="hidden" name="type" id="type${room.id}">
                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle"
                                                    type="button"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="true">
                                                    ${room.type}
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="javascript:{}" onclick="
                                                            document.getElementById('type${room.id}').setAttribute('value', 'double');
                                                            document.getElementById('${room.id}').submit();">
                                                        <fmt:message key="room.type.double"/>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:{}" onclick="
                                                            document.getElementById('type${room.id}').setAttribute('value', 'family');
                                                            document.getElementById('${room.id}').submit();">
                                                        <fmt:message key="room.type.family"/>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:{}" onclick="
                                                            document.getElementById('type${room.id}').setAttribute('value', 'single');
                                                            document.getElementById('${room.id}').submit();">
                                                        <fmt:message key="room.type.single"/>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </form>
                                </td>
                                <td style="vertical-align: middle">
                                    <a href="javascript:{}" onclick="document.getElementById('${room.id}').submit();">
                                        <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

                <ul class="pagination pagination-lg">
                        <c:choose>
                            <c:when test="${ sessionScope.curPage > 1 }">
                                <c:set var="prev">
                                    <c:url value="/controller">
                                        <c:param name="command" value="admin_rooms"/>
                                        <c:param name="page" value="${sessionScope.curPage - 1}"/>
                                    </c:url>
                                </c:set>
                                <li>
                                    <a href="${prev}" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="disabled"><a href="#">&laquo;</a></li>
                            </c:otherwise>
                        </c:choose>
                        <c:forEach begin="1" end="${ sessionScope.pages }" var="i">
                            <c:choose>
                                <c:when test="${ sessionScope.curPage eq i}">
                                    <li class="active"><a href="#"> ${ i } </a></li>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="page">
                                        <c:url value="/controller">
                                            <c:param name="command" value="admin_rooms"/>
                                            <c:param name="page" value="${i}"/>
                                        </c:url>
                                    </c:set>
                                    <li><a href="${page}">${i}</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:choose>
                            <c:when test="${ sessionScope.curPage < sessionScope.pages }">
                                <c:set var="next">
                                    <c:url value="/controller">
                                        <c:param name="command" value="admin_rooms"/>
                                        <c:param name="page" value="${sessionScope.curPage + 1}"/>
                                    </c:url>
                                </c:set>
                                <li>
                                    <a href="${next}" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="disabled"><a href="#">&raquo;</a></li>
                            </c:otherwise>
                        </c:choose>
                    </ul>

            </div>
            <%--Footer--%>
            <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </main>
    </body>
</html>
</fmt:bundle>